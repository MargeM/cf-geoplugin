<?php
/**
 * wp-admin navigations and actions
 *
 * @link      http://cfgeoplugin.com/
 * @since      3.1.1
 *
 * @package    CF_Geoplugin
 * @subpackage CF_Geoplugin/admin/include
 */

/**
* wp-admin plugin navigation and actions
* @version   1.0.0
*/
add_action('admin_menu', 'cf_geoplugin_admin_menu', 10);
function cf_geoplugin_admin_menu(){
	add_menu_page(
		__('CF GeoPlugin',WP_CF_GEO_PLUGIN_NAME),
		__('CF GeoPlugin',WP_CF_GEO_PLUGIN_NAME),
		'manage_options',
		'cf-geoplugin',
		'cf_geoplugin_page_geoplugin',
		plugin_dir_url( dirname( __FILE__ ) ) . 'images/main-menu.png'
	);
	$cf_geo_enable_gmap=get_option("cf_geo_enable_gmap");
	if($cf_geo_enable_gmap == 'true')
	{
		add_submenu_page(
			'cf-geoplugin',
			__('Google Map',WP_CF_GEO_PLUGIN_NAME),
			__('Google Map',WP_CF_GEO_PLUGIN_NAME),
			'manage_options',
			'cf-geoplugin-google-map',
			'cf_geoplugin_page_google_map'
		);
	}
	if ( current_user_can( 'edit_pages' ) && current_user_can( 'edit_posts' ) ) {
		$cf_geo_enable_defender=get_option("cf_geo_enable_defender");
		if($cf_geo_enable_defender == 'true')
		{
			add_submenu_page(
				'cf-geoplugin',
				__('Geo Defender',WP_CF_GEO_PLUGIN_NAME),
				__('Geo Defender',WP_CF_GEO_PLUGIN_NAME),
				'manage_options',
				'cf-geoplugin-defender',
				'cf_geoplugin_page_defender'
			);
		}
		add_submenu_page(
			'cf-geoplugin',
			__('Debug Mode',WP_CF_GEO_PLUGIN_NAME),
			__('Debug Mode',WP_CF_GEO_PLUGIN_NAME),
			'manage_options',
			'cf-geoplugin-debug',
			'cf_geoplugin_page_debug'
		);
		add_submenu_page(
			'cf-geoplugin',
			__('Settings',WP_CF_GEO_PLUGIN_NAME),
			__('Settings',WP_CF_GEO_PLUGIN_NAME),
			'manage_options',
			'cf-geoplugin-settings',
			'cf_geoplugin_page_settings'
		);
		/*add_submenu_page(
			'cf-geoplugin',
			__('Credits & Info',WP_CF_GEO_PLUGIN_NAME),
			__('Credits & Info',WP_CF_GEO_PLUGIN_NAME),
			'manage_options',
			'cf-geoplugin-credits',
			'cf_geoplugin_page_credits'
		);*/
	}
}
function cf_geoplugin_page_geoplugin() {
	include_once( plugin_dir_path( dirname( __FILE__ ) ) . 'pages/page-geoplugin.php');
}
function cf_geoplugin_page_google_map() {
	include_once( plugin_dir_path( dirname( __FILE__ ) ) . 'pages/page-google-map.php');
}
function cf_geoplugin_page_defender() {
	if ( current_user_can( 'edit_pages' ) && current_user_can( 'edit_posts' ) ) include_once( plugin_dir_path( dirname( __FILE__ ) ) . 'pages/page-defender.php');
}
function cf_geoplugin_page_debug() {
	if ( current_user_can( 'edit_pages' ) && current_user_can( 'edit_posts' ) ) include_once( plugin_dir_path( dirname( __FILE__ ) ) . 'pages/page-debug.php');
}
function cf_geoplugin_page_settings() {
	if ( current_user_can( 'edit_pages' ) && current_user_can( 'edit_posts' ) ) include_once( plugin_dir_path( dirname( __FILE__ ) ) . 'pages/page-settings.php');
};
/*function cf_geoplugin_page_credits() {
	if ( current_user_can( 'edit_pages' ) && current_user_can( 'edit_posts' ) ) include_once( plugin_dir_path( dirname( __FILE__ ) ) . 'pages/page-credits.php');
};*/

/**
* Add admin bar menu
*/
add_action( 'admin_bar_menu', 'cf_geoplugin_admin_bar_menu', 900 );
function cf_geoplugin_admin_bar_menu() {
	global $wp_admin_bar;
    $wp_admin_bar->add_node( array(
        'id' => 'cf-geoplugin',
        'title' => '<span  style="
    float:left; width:25px !important; height:25px !important;
    margin-left: 5px !important; margin-top: 2px !important; background:url(\''.plugin_dir_url( dirname( __FILE__ ) ).'images/cf-geo-25x25.png\') no-repeat center center / cover;"></span>',
        'href' => '',
		'meta'  => array( 'class' => 'cf-geoplugin-toolbar-page', 'title'=>sprintf(__("CF GeoPlugin ver.%s",WP_CF_GEO_PLUGIN_NAME),do_shortcode("[cf_geo return=version]"))),
		'parent' => false,
    ) );
	$wp_admin_bar->add_node( array(
        'id' => 'cf-geoplugin-helper',
        'title' => 'CF GeoPlugin',
        'href' => admin_url( 'admin.php?page=cf-geoplugin'),
		'meta'  => array( 'class' => 'cf-geoplugin-toolbar-help-page' ),
		'parent' => 'cf-geoplugin',
    ) );
	$cf_geo_enable_gmap=get_option("cf_geo_enable_gmap");
	if($cf_geo_enable_gmap == 'true')
	{
		$wp_admin_bar->add_node( array(
			'id' => 'cf-geoplugin-gmap',
			'title' => __('CF Google Map',WP_CF_GEO_PLUGIN_NAME),
			'href' => admin_url( 'admin.php?page=cf-geoplugin-google-map'),
			'meta'  => array( 'class' => 'cf-geoplugin-gmap-toolbar-page' ),
			'parent' => 'cf-geoplugin',
		) );
	}
	if ( current_user_can( 'edit_pages' ) && current_user_can( 'edit_posts' ) ) {
		$cf_geo_enable_defender=get_option("cf_geo_enable_defender");
		if($cf_geo_enable_defender == 'true')
		{
			$wp_admin_bar->add_node( array(
				'id' => 'cf-geoplugin-defender',
				'title' => __('CF Geo Defender',WP_CF_GEO_PLUGIN_NAME),
				'href' => admin_url( 'admin.php?page=cf-geoplugin-defender'),
				'meta'  => array( 'class' => 'cf-geoplugin-defender-toolbar-page' ),
				'parent' => 'cf-geoplugin',
			) );
		}
		$wp_admin_bar->add_node( array(
			'id' => 'cf-geoplugin-debug',
			'title' => __('Debug Mode',WP_CF_GEO_PLUGIN_NAME),
			'href' => admin_url( 'admin.php?page=cf-geoplugin-debug'),
			'meta'  => array( 'class' => 'cf-geoplugin-debug-toolbar-page' ),
			'parent' => 'cf-geoplugin',
		) );
		$wp_admin_bar->add_node( array(
			'id' => 'cf-geoplugin-setup',
			'title' => __('Settings',WP_CF_GEO_PLUGIN_NAME),
			'href' => admin_url( 'admin.php?page=cf-geoplugin-settings'),
			'meta'  => array( 'class' => 'cf-geoplugin-setup-toolbar-page' ),
			'parent' => 'cf-geoplugin',
		) );
	}
	$wp_admin_bar->add_node( array(
		'id' => 'cf-geoplugin-devider',
		'title' => '<span style="text-align:center; display:block;width:100%;">------------ '.__("Info",WP_CF_GEO_PLUGIN_NAME).' ------------</span>',
		'href' => '',
		'parent' => 'cf-geoplugin',
	) );
	/* Include CF Geop Init class */
	$init=new CF_Geoplugin;
	/* Get IP */
	$ip=$init->ip();
	if(in_array($ip,$init->ip_blocked()))
		$wp_admin_bar->add_node( array(
			'id' => 'cf-geoplugin-info',
			'title' => __("Your IP: 0.0.0.0",WP_CF_GEO_PLUGIN_NAME),
			'href' => '',
			'parent' => 'cf-geoplugin',
		) );
	else
		$wp_admin_bar->add_node( array(
			'id' => 'cf-geoplugin-info',
			'title' => '<em>'.__("Your IP",WP_CF_GEO_PLUGIN_NAME).': </em>'.do_shortcode("[cf_geo return=ip]").' ('.do_shortcode("[cf_geo return=ip_version]").')',
			'href' => '',
			'parent' => 'cf-geoplugin',
		) );
	$address = do_shortcode("[cf_geo return=address default='']");
	if(!empty($address))
	{
		
		$wp_admin_bar->add_node( array(
			'id' => 'cf-geoplugin-info-city',
			'title' => '<em>'.__("City",WP_CF_GEO_PLUGIN_NAME).':</em> '.do_shortcode("[cf_geo return=city default='-']"),
			'href' => '',
			'parent' => 'cf-geoplugin-info',
		) );
		$wp_admin_bar->add_node( array(
			'id' => 'cf-geoplugin-info-region',
			'title' => '<em>'.__("Region",WP_CF_GEO_PLUGIN_NAME).':</em> '.do_shortcode("[cf_geo return=region default='-']"),
			'href' => '',
			'parent' => 'cf-geoplugin-info',
		) );
		$wp_admin_bar->add_node( array(
			'id' => 'cf-geoplugin-info-country',
			'title' => '<em>'.__("Country",WP_CF_GEO_PLUGIN_NAME).':</em> '.do_shortcode("[cf_geo return=country default='-']"),
			'href' => '',
			'parent' => 'cf-geoplugin-info',
		) );
		$wp_admin_bar->add_node( array(
			'id' => 'cf-geoplugin-info-continent',
			'title' => '<em>'.__("Continent",WP_CF_GEO_PLUGIN_NAME).':</em> '.do_shortcode("[cf_geo return=continent default='-']"),
			'href' => '',
			'parent' => 'cf-geoplugin-info',
		) );
		$wp_admin_bar->add_node( array(
			'id' => 'cf-geoplugin-info-timezone',
			'title' => '<em>'.__("Timezone",WP_CF_GEO_PLUGIN_NAME).':</em> '.do_shortcode("[cf_geo return=timezone default='-']"),
			'href' => '',
			'parent' => 'cf-geoplugin-info',
		) );
		$wp_admin_bar->add_node( array(
			'id' => 'cf-geoplugin-info-status',
			'title' => '<em>'.__("Status",WP_CF_GEO_PLUGIN_NAME).':</em> '.do_shortcode("[cf_geo return=status]"),
			'href' => '',
			'parent' => 'cf-geoplugin-info',
		) );
	}
}

/* Plugin page buttons */
add_filter( 'plugin_action_links', 'cf_geoplugin_add_action_plugin', 10, 5 );
function cf_geoplugin_add_action_plugin( $actions, $plugin_file ) 
{
	static $plugin;

	if (!isset($plugin))
		$plugin = plugin_basename(WP_CF_GEO_PLUGIN_FILE);
	if ($plugin == $plugin_file)
	{
		$settings = array('settings' => '<i class="fa fa-cog fa-spin"></i> <a href="'.admin_url( 'admin.php?page=cf-geoplugin-settings').'" target="_self" rel="noopener noreferrer">Settings</a>');
		$faq = array('faq' => '<i class="fa fa-question-circle-o"></i> <a href="http://cfgeoplugin.com/faq" target="_blank" rel="noopener noreferrer">FAQ</a>');
		$vote = array('vote' => '<i class="fa fa-star fa-spin"></i> <a href="https://wordpress.org/support/plugin/cf-geoplugin/reviews/#new-post" target="_blank" rel="noopener noreferrer">Vote</a>');
		$donate = array('donate' => '<i class="fa fa-heartbeat"></i> <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=creativform@gmail.com" target="_blank" rel="noopener noreferrer">Donate</a>');
	
		
		$actions = array_merge($faq, $actions);	
		$actions = array_merge($vote, $actions);
		$actions = array_merge($donate, $actions);
		$actions = array_merge($settings, $actions);
	}		
	return $actions;
}