<div class="postbox-container" id="postbox-container-1">
	<?php
		$data = $init->get_data("http://cfgeoplugin.com/team.json");
		$data = json_decode($data);
		$defender = new CF_Geoplugin_Defender;
		$enable=$defender->enable;
	//	var_dump($data);
	?>
	<?php if($enable==false): ?>
	<div class="postbox">
		<h2 class="hndle ui-sortable-handle"><span><span class="fa fa-star-o"></span> <?php echo __("Get CF GeoPlugin PRO!",WP_CF_GEO_PLUGIN_NAME); ?></span></h2>
		<div class="inside">
			<p><?php echo sprintf(__("Full functions are only enabled in PRO version. Don't worry, we setup for you optimal settings.%sIf you want to enable all options like CF Geo Banner, Country Flags, Cloudflare, DNS Lookup, SSL, Proxy and use full functionality of CF Geo Plugin, you can do it for low as $%s with the %s.",WP_CF_GEO_PLUGIN_NAME),'<br><br>',WP_CF_GEO_PLUGIN_PREMIUM_PRICE, '<strong>lifetime license and support</strong>'); ?></p>
			<ul>
				<li><h3><?php echo __("PRO Features:",WP_CF_GEO_PLUGIN_NAME); ?></h3></li>
				<li><span class="fa fa-check" aria-hidden="true"></span> <?php echo __("Cloudflare Support",WP_CF_GEO_PLUGIN_NAME); ?></li>
				<li><span class="fa fa-check" aria-hidden="true"></span> <?php echo __("Proxy Settings",WP_CF_GEO_PLUGIN_NAME); ?></li>
				<li><span class="fa fa-check" aria-hidden="true"></span> <?php echo __("DNS Lookup",WP_CF_GEO_PLUGIN_NAME); ?></li>
				<li><span class="fa fa-check" aria-hidden="true"></span> <?php echo __("SSL",WP_CF_GEO_PLUGIN_NAME); ?></li>
				<li><span class="fa fa-check" aria-hidden="true"></span> <?php echo __("CF Geo Banner",WP_CF_GEO_PLUGIN_NAME); ?></li>
				<li><span class="fa fa-check" aria-hidden="true"></span> <?php echo __("Country Flag Support",WP_CF_GEO_PLUGIN_NAME); ?></li>
				<li><span class="fa fa-check" aria-hidden="true"></span> <?php echo __("Google Map Global Settings",WP_CF_GEO_PLUGIN_NAME); ?></li>
				<li><span class="fa fa-check" aria-hidden="true"></span> <?php echo __("CF Geo Defender Full Functionality",WP_CF_GEO_PLUGIN_NAME); ?></li>
			</ul>
			<br><br>
			<a href="<?php echo get_admin_url(); ?>/admin.php?page=cf-geoplugin-settings" style="display:block; width:100%;">
				<img alt="PayPal - The safer, easier way to pay online!" src="<?php echo WP_CF_GEO_PLUGIN_URL; ?>/admin/images/shop.jpg" style="margin:0 auto;display:block;">
			</a>
		</div>
	</div>
	<?php endif; ?>
	<?php if(isset($data->sponsors)): ?>
	<div class="postbox">
		<h2 class="hndle ui-sortable-handle"><span><span class="fa fa-star-o"></span> <?php echo __('Valuable Sponsors',WP_CF_GEO_PLUGIN_NAME); ?></span></h2>
		<div class="inside">
			<?php echo sprintf(__("Meet our valuable sponsors who support us and help our development team. If you like to be our sponsor and your logo appears here please contact us %s", WP_CF_GEO_PLUGIN_NAME),'<a href="mailto:creativform@gmail.com">creativform@gmail.com</a>'); ?>
			<?php
				foreach($data->sponsors as $c)
				{
					echo sprintf('<a href="%s" target="_blank" id="sponsor" title="%s" style="%s"><img src="%s" alt="%s" style="%s"></a>', $c->url,$c->title,'display:block; width:100%;',$c->img,$c->title,'margin:10px auto; display:block; width:100%; max-width:150px;');
				}
			?>
		</div>
	</div>
	<?php endif; ?>
<!--	<div class="postbox">
		<h2 class="hndle ui-sortable-handle"><span><span class="fa fa-heartbeat"></span> <?php echo __('Donate',WP_CF_GEO_PLUGIN_NAME); ?></span></h2>
		<div class="inside">
		<?php echo sprintf(__("Our plugin is for now free, but the work on it cost time and money.%sIf you really like this plugin, we will continue to develop and update it. You can donate some money to our development team because in the future we plan to improve this plugin and add new functions and better user experience.%s Thank you for your concern.%s Sincerely, your %s",WP_CF_GEO_PLUGIN_NAME),'<br><br>','<br><br>','<br><br>', '<a href="http://cfgeoplugin.com" target="_blank">'.__('CF GeoPlugin team',WP_CF_GEO_PLUGIN_NAME).'</a>'); ?>
		<br><br>
			<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank">

				<input type="hidden" name="business"
					value="creativform@gmail.com">
				

				<input type="hidden" name="cmd" value="_donations">
				

				<input type="hidden" name="item_name" value="<?php echo __('CF GeoPlugin Donation',WP_CF_GEO_PLUGIN_NAME); ?>">
				<input type="hidden" name="item_number" value="<?php echo __('Donation to CF team for the improvement and maintenance of CF GeoPlugin',WP_CF_GEO_PLUGIN_NAME); ?>">
				<input type="hidden" name="currency_code" value="USD">

				

				<input type="image" name="submit" border="0"
				src="https://www.paypalobjects.com/en_US/i/btn/btn_donate_LG.gif"
				alt="PayPal - The safer, easier way to pay online" style="margin:10px auto; display:block;">
				<img alt="" border="0" width="1" height="1" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif">
			
			</form>
		</div>
	</div> -->
	<?php if(isset($data->info) && count($data->info)>0): ?>
	<div class="postbox">
		<h2 class="hndle ui-sortable-handle"><span><span class="fa fa-info"></span> <?php echo __('Live News & Info',WP_CF_GEO_PLUGIN_NAME); ?></span></h2>
		<div class="inside">
			 <?php
				foreach($data->info as $i)
				{
					echo sprintf('<p><h4>%s</h4>%s<br><small>~%s</small></p>',$i->title, $i->text, $i->date);
				}
			?>
		</div>
	</div>
	<?php endif; ?>
</div>