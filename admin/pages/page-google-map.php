<?php
if (isset($_POST) && count($_POST)>0) {
	// Do the saving
	$front_page_elements = array();

	foreach($_POST as $key=>$val){
		update_option($key, esc_attr($val));
	}
}
?>
<div class="wrap">
    <h1><span class="fa fa-globe"></span> <?php echo __('CF Google Map',WP_CF_GEO_PLUGIN_NAME); ?></h1>
    <p><?php echo __('Google Maps is a desktop web mapping service developed by Google. It offers satellite imagery, street maps, 360° panoramic views of streets (Street View), real-time traffic conditions (Google Traffic), and route planning for traveling by foot, car, bicycle (in beta), or public transportation. CF GeoPlugin allow you to place google map easy in your WordPress blog using simple shortcode.',WP_CF_GEO_PLUGIN_NAME); ?></p>
    
    <h2 class="nav-tab-wrapper">
    	<a class="nav-tab nav-tab-active" href="#property"><span class="fa fa-cog"></span> <?php echo __('Property List',WP_CF_GEO_PLUGIN_NAME); ?></a>
        <a class="nav-tab" href="#info"><span class="fa fa-info"></span> <?php echo __('Info & Examples',WP_CF_GEO_PLUGIN_NAME); ?></a>
    </h2>
    
    <div class="nav-tab-body">
    	<div class="nav-tab-item nav-tab-item-active" id="property">
        	<h3>Property List</h3>
            <table width="100%" class="wp-list-table widefat fixed striped pages">
                <thead>
                    <tr>
                        <th class="manage-column column-shortcode column-primary" width="30%"><strong><?php echo __('Name',WP_CF_GEO_PLUGIN_NAME); ?></strong></th>
                        <th class="manage-column column-returns column-primary"><strong><?php echo __('Info',WP_CF_GEO_PLUGIN_NAME); ?></strong></th></tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="code">title</td>
                        <td><strong><?php echo __('String',WP_CF_GEO_PLUGIN_NAME); ?></strong> - <?php echo __('Mouse hover title',WP_CF_GEO_PLUGIN_NAME); ?></td>
                    </tr>
                    <tr>
                        <td class="code">latitude</td>
                        <td><strong><?php echo __('Number',WP_CF_GEO_PLUGIN_NAME); ?></strong> - <?php echo sprintf(__('Latitude is an angle (defined below) which ranges from 0° at the Equator to 90° (North or South) at the poles.%s',WP_CF_GEO_PLUGIN_NAME),'<br><br><strong>-'.__('By default is pointed to visitors city or address automaticly.',WP_CF_GEO_PLUGIN_NAME).'</strong>'); ?>
				  </td>
        </tr>
                    <tr>
                        <td class="code">longitude</td>
                        <td><strong><?php echo __('Number',WP_CF_GEO_PLUGIN_NAME); ?></strong> - <?php echo __('Longitude (shown as a vertical line) is the angular distance, in degrees, minutes, and seconds, of a point east or west of the Prime (Greenwich) Meridian.',WP_CF_GEO_PLUGIN_NAME); ?><br><br><strong>-<?php echo __('By default is pointed to visitors city or address automaticly',WP_CF_GEO_PLUGIN_NAME); ?></strong></td>
        </tr>
                    <tr>
                        <td class="code">zoom</td>
                        <td><strong><?php echo __('Integer',WP_CF_GEO_PLUGIN_NAME); ?></strong> - <?php echo __('Most roadmap imagery is available from zoom levels 0 to 18.',WP_CF_GEO_PLUGIN_NAME); ?><br><br><strong>-<?php echo __('Default is',WP_CF_GEO_PLUGIN_NAME); ?> <?php echo get_option("cf_geo_map_zoom"); ?></strong></td>
        </tr>
                    <tr>
                        <td class="code">width</td>
                        <td><strong><?php echo __('Accept numeric value in percentage or pixels (% or px)',WP_CF_GEO_PLUGIN_NAME); ?></strong> - <?php echo __('Width of your map.',WP_CF_GEO_PLUGIN_NAME); ?><br><br><strong>-<?php echo __('Default is',WP_CF_GEO_PLUGIN_NAME); ?> <?php echo (get_option("cf_geo_map_width")); ?>.</strong></td>
        </tr>
                    <tr>
                        <td class="code">height</td>
                        <td><strong><?php echo __('Accept numeric value in percentage or pixels (% or px)',WP_CF_GEO_PLUGIN_NAME); ?></strong> - <?php echo __('Height of your map.',WP_CF_GEO_PLUGIN_NAME); ?><br><br><strong>-<?php echo __('Default is',WP_CF_GEO_PLUGIN_NAME); ?> <?php echo (get_option("cf_geo_map_height")); ?>.</strong></td>
        </tr>
                    <tr>
                        <td class="code">scrollwheel</td>
                        <td><strong><?php echo __('Integer 1 or 0',WP_CF_GEO_PLUGIN_NAME); ?></strong> - <?php echo __('If',WP_CF_GEO_PLUGIN_NAME); ?> <em>0</em>, <?php echo __('disables scrollwheel zooming on the map.',WP_CF_GEO_PLUGIN_NAME); ?>',WP_CF_GEO_PLUGIN_NAME); ?><br><br><strong>-<?php echo __('Default is',WP_CF_GEO_PLUGIN_NAME); ?> <?php echo get_option("cf_geo_map_scrollwheel"); ?></strong></td>
        </tr>
                    <tr>
                        <td class="code">navigationControl</td>
                        <td><strong><?php echo __('Integer 1 or 0',WP_CF_GEO_PLUGIN_NAME); ?></strong> - <?php echo __('If',WP_CF_GEO_PLUGIN_NAME); ?> <em>0</em>, <?php echo __('disables navigation on the map. The initial enabled/disabled state of the Map type control.',WP_CF_GEO_PLUGIN_NAME); ?><br><br><strong>-<?php echo __('Default is',WP_CF_GEO_PLUGIN_NAME); ?> <?php echo get_option("cf_geo_map_navigationControl"); ?></strong></td>
        </tr>
                    <tr>
                        <td class="code">mapTypeControl</td>
                        <td><strong><?php echo __('Integer 1 or 0',WP_CF_GEO_PLUGIN_NAME); ?></strong> - <?php echo __('The initial enabled/disabled state of the Map type control.',WP_CF_GEO_PLUGIN_NAME); ?><br><br><strong>-<?php echo __('Default is',WP_CF_GEO_PLUGIN_NAME); ?> <?php echo get_option("cf_geo_map_scaleControl"); ?></strong></td>
        </tr>
                    <tr>
                        <td class="code">scaleControl</td>
                        <td><strong><?php echo __('Integer 1 or 0',WP_CF_GEO_PLUGIN_NAME); ?></strong> - <?php echo __('The initial display options for the Scale control.',WP_CF_GEO_PLUGIN_NAME); ?><br><br><strong>-<?php echo __('Default is',WP_CF_GEO_PLUGIN_NAME); ?> <?php echo get_option("cf_geo_map_mapTypeControl"); ?></strong></td>
        </tr>
                    <tr>
                        <td class="code">draggable</td>
                        <td><strong><?php echo __('Integer 1 or 0',WP_CF_GEO_PLUGIN_NAME); ?></strong> - <?php echo __('If',WP_CF_GEO_PLUGIN_NAME); ?> <em>0</em>, <?php echo __('the object can be dragged across the map and the underlying feature will have its geometry updated.',WP_CF_GEO_PLUGIN_NAME); ?> <br><br><strong>-<?php echo __('Default is',WP_CF_GEO_PLUGIN_NAME); ?> <?php echo get_option("cf_geo_map_draggable"); ?>.</strong></td>
        </tr>
                    <tr>
                        <td class="code">infoMaxWidth</td>
                        <td><strong><?php echo __('Integer from 0 to 600',WP_CF_GEO_PLUGIN_NAME); ?></strong> - <?php echo __('Maximum width of info popup inside map.',WP_CF_GEO_PLUGIN_NAME); ?><br><br><strong>-<?php echo __('Default is',WP_CF_GEO_PLUGIN_NAME); ?> <?php echo get_option("cf_geo_map_infoMaxWidth"); ?></strong></td>
        </tr>
                </tbody>
           </table>
        </div>
        <div class="nav-tab-item nav-tab-item-active" id="info">
        	<h3><?php echo __('Adding Google Map in wordpress',WP_CF_GEO_PLUGIN_NAME); ?></h3>
            <p class="manage-menus">
			<?php echo sprintf(__("If you whant to place simple google map in your post or page, you just need to place shortcode %s and your visitor will see own city on google map by default. This shortcode have also own properties what you can use to customize your Google map (look property list).",WP_CF_GEO_PLUGIN_NAME),'<code>[cf_geo_map]</code>'); ?>
            <br><br>		
			<?php echo sprintf(__("Like example, you can display own company street address inside Google map like this: %s and pointer will show your street and place where you work.",WP_CF_GEO_PLUGIN_NAME),'<code>[cf_geo_map longitude="-74.0059" latitude="40.7128" zoom="15"]</code>'); ?>
            <br><br>
            <?php echo __('Google map also allow you to use HTML inside map and display info bar:',WP_CF_GEO_PLUGIN_NAME); ?>
            <br><br>
            <code>[cf_geo_map longitude="-74.0059" latitude="40.7128" zoom="15" title="<?php echo __('My Company Name',WP_CF_GEO_PLUGIN_NAME); ?>"]&nbsp;<br>
                &nbsp;&nbsp;&nbsp;&lt;h3&gt;<?php echo __('My Company Name',WP_CF_GEO_PLUGIN_NAME); ?>&lt;h3&gt;&nbsp;<br>
                &nbsp;&nbsp;&nbsp;&lt;p&gt;<?php echo __('No Name Street 35, New York, USA',WP_CF_GEO_PLUGIN_NAME); ?>&lt;/p&gt;&nbsp;<br>
                &nbsp;&nbsp;&nbsp;&lt;p&gt;<?php echo __('We have what you need',WP_CF_GEO_PLUGIN_NAME); ?>&lt;/p&gt;&nbsp;<br>
            [/cf_geo_map]&nbsp;</code><br><br>
            <?php echo __('With this plugin you can easy setup your google map.',WP_CF_GEO_PLUGIN_NAME); ?>
            </p>
        </div>
    </div>
	<?php include plugin_dir_path( __FILE__ ) . 'page-settings/settings-donation.php'; ?>
   <?php 
   	$defender = new CF_Geoplugin_Defender;
	$enable=$defender->enable;
   if($enable==true) : ?>
    <?php echo do_shortcode("[cf_geo_map]<h4>Demo Map</h4><p>".do_shortcode('[cf_geo return="address"]')."</p>[/cf_geo_map]"); ?>
    <?php endif; ?>
   
</div>