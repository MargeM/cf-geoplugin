<h3><span class="fa fa-info-circle"></span> <?php echo __('Credits',WP_CF_GEO_PLUGIN_NAME); ?></h3>
<p><?php echo sprintf(__('%s, %s, %s and all this functionality is made by %s with help of his %s.',WP_CF_GEO_PLUGIN_NAME),'CF Geoplugin','CF Geoplugin API','CF Geo Banner','<a href="https://www.linkedin.com/in/ivijanstefanstipic" target="_blank"><em><strong>Ivijan-Stefan Stipic</strong></em></a>',(isset($data->team)?'<a href="'.admin_url('admin.php?page=cf-geoplugin-settings&part=team-members').'" target="_self"><em><strong>'.__('team',WP_CF_GEO_PLUGIN_NAME).'</strong></em></a>':__('team',WP_CF_GEO_PLUGIN_NAME))); ?></p>
<p><strong><?php echo __('This plugin have purpose to help business people to reach own goals and made maximal conversions. Plugin is SEO optimized, user friendly, easy for use and very dynamic.',WP_CF_GEO_PLUGIN_NAME); ?></strong></p> 

<p><?php echo sprintf(__('The WordPress plugin %s is %s plugin which allows you to attach geographic information and Google maps to posts, pages, widgets and custom templates by using shortcodes and user IP address. It also lets you to specify a default geographic location for your entire WordPress blog. Also you can place banners and content realated to user locations.',WP_CF_GEO_PLUGIN_NAME),'<strong>CF GeoPlugin</strong>', '<strong>FREE</strong>'); ?></p>

<h3><span class="fa fa-star-o"></span> <?php echo __('PRO Package',WP_CF_GEO_PLUGIN_NAME); ?></h3>
<p><?php echo sprintf(__('%s functionality enable some additional and special options inside this plugin. This plugin is made for your needs and because of that, by purchasing a PRO package you also helping the further development and maintaince of this plugin.',WP_CF_GEO_PLUGIN_NAME),'<strong>'.__('PRO',WP_CF_GEO_PLUGIN_NAME).'</strong>'); ?></p>

<h3><span class="fa fa-code"></span> <?php echo __('For Developers and Contributors',WP_CF_GEO_PLUGIN_NAME); ?></h3>
<p><?php echo __('You are welcome to contribute to my plugin and help me in the development. Also you are welcome to use this plugin inside your projects like part of your own plugins and templates. All great new ideas will be taken into consideration and all together we can do something fantastic!',WP_CF_GEO_PLUGIN_NAME); ?></p>

<h3><span class="fa fa-copyright"></span> <?php echo __('Copyright',WP_CF_GEO_PLUGIN_NAME); ?></h3>
<p>Copyright <span class="fa fa-copyright"></span> 2015 - <?php echo date("Y")?> <a href="http://cfgeoplugin.com" target="_blank"><em><strong>CF GeoPlugin</strong></em></a> by <a href="https://www.linkedin.com/in/ivijanstefanstipic" target="_blank"><em><strong>Ivijan-Stefan Stipic</strong></em></a>. All Right Reserved.</p>
<p>This program is free software; you can redistribute it and/or<br>
modify it under the terms of the GNU General Public License<br>
as published by the Free Software Foundation; either version 2<br>
of the License, or (at your option) any later version.</p>
<p>This program is distributed in the hope that it will be useful,<br>
but WITHOUT ANY WARRANTY; without even the implied warranty of<br>
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.</p>
<a href="javascript:void(0);" onClick="cf_geoplugin_popup('<?php echo plugin_dir_url(dirname(dirname(dirname(__FILE__))))?>LICENSE.txt','GNU GENERAL PUBLIC LICENSE','550','450');">See the GNU General Public License for more details</a>.</p>
<p>You should have received a copy of the GNU General Public License<br>
along with this program; if not, write to the Free Software<br>
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.</p>