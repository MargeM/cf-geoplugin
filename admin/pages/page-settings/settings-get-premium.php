<?php
$defender = new CF_Geoplugin_Defender;
$enable=$defender->enable;
?>
<?php if($enable==false): ?>
    <div class="manage-menus">
    	<h3><?php echo __("Get CF GeoPlugin PRO!",WP_CF_GEO_PLUGIN_NAME); ?></h3> <?php echo sprintf(__("Full functions of this plugin are only enabled in PRO version. Don't worry, we set up for you optimal settings.%sIf you want to enable all options like CF Geo Banner, Country Flags, Cloudflare, DNS Lookup, SSL, Proxy, Country SEO Redirection and use full functionality of CF Geo Plugin, you can do it for low as $%s with the %s.",WP_CF_GEO_PLUGIN_NAME),'<br>',WP_CF_GEO_PLUGIN_PREMIUM_PRICE, '<strong>lifetime license and support</strong>'); ?>
		<ul>
			<li class="title"><h3><?php echo __("PRO Features:",WP_CF_GEO_PLUGIN_NAME); ?></h3></li>
			<li><span class="fa fa-check" aria-hidden="true"></span> <?php echo __("Cloudflare Geolocation Support",WP_CF_GEO_PLUGIN_NAME); ?></li>
			<li><span class="fa fa-check" aria-hidden="true"></span> <?php echo __("Proxy Settings",WP_CF_GEO_PLUGIN_NAME); ?></li>
			<li><span class="fa fa-check" aria-hidden="true"></span> <?php echo __("DNS Lookup",WP_CF_GEO_PLUGIN_NAME); ?></li>
			<li><span class="fa fa-check" aria-hidden="true"></span> <?php echo __("SSL",WP_CF_GEO_PLUGIN_NAME); ?></li>
			<li><span class="fa fa-check" aria-hidden="true"></span> <?php echo __("IP Version Lookup",WP_CF_GEO_PLUGIN_NAME); ?></li>
			<li><span class="fa fa-check" aria-hidden="true"></span> <?php echo __("CF Geo Banner",WP_CF_GEO_PLUGIN_NAME); ?></li>
			<li><span class="fa fa-check" aria-hidden="true"></span> <?php echo __("Country SEO Redirection",WP_CF_GEO_PLUGIN_NAME); ?></li>
			<li><span class="fa fa-check" aria-hidden="true"></span> <?php echo __("Country Flag Support",WP_CF_GEO_PLUGIN_NAME); ?></li>
			<li><span class="fa fa-check" aria-hidden="true"></span> <?php echo __("Google Map Global Settings",WP_CF_GEO_PLUGIN_NAME); ?></li>
			<li><span class="fa fa-check" aria-hidden="true"></span> <?php echo __("CF Geo Defender Full Functionality",WP_CF_GEO_PLUGIN_NAME); ?></li>
			<li><span class="fa fa-check" aria-hidden="true"></span> <?php echo __("Include/Exclude by Geolocation Functionality",WP_CF_GEO_PLUGIN_NAME); ?></li>
			<li><span class="fa fa-check" aria-hidden="true"></span> <?php echo __("Lifetime Lycense, Support & Updaes",WP_CF_GEO_PLUGIN_NAME); ?></li>
		</ul>
		<br><br><form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="YAHB7JXXVLZUQ">
<input type="image" src="<?php echo WP_CF_GEO_PLUGIN_URL; ?>/admin/images/shop.jpg" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
</form>
    </div>
<?php endif; ?>
<form method="post" enctype="multipart/form-data" action="<?php echo  $url->url; ?>" target="_self" id="settings-form">
<?php if($enable==false);