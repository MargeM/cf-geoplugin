<?php
$config=new CF_Geoplugin;
$url=$config->url();

$defender = new CF_Geoplugin_Defender;
$enable=$defender->enable;

$enableForm = ($enable==false ? ' disabled':'');
?>
<h3><span class="fa fa-cog"></span> <?php echo __('Global Plugin Settings',WP_CF_GEO_PLUGIN_NAME); ?></h3>
<?php if($enable==false): ?>
	<?php require_once plugin_dir_path(__FILE__) . '/settings-get-premium.php'; ?>
<?php endif; ?>
<form method="post" enctype="multipart/form-data" action="<?php echo  $url->url; ?>" target="_self" id="settings-form">
<?php if($enable==false): ?>
	<table class="form-table manage-menus">
    	<tbody>
        	<tr>
            	<th scope="row" style="text-align:right">
                	<label for="cf_geo_defender_api_key"><?php echo __('Activation KEY',WP_CF_GEO_PLUGIN_NAME); ?>:</label>
                </th>
                <td>
                	<input type="text" autocomplete="off" value="" name="cf_geo_defender_api_key" id="cf_geo_defender_api_key"><input type="submit" value="<?php echo __('ACTIVATE',WP_CF_GEO_PLUGIN_NAME); ?>" class="button action">
                </td>
            </tr>
        </tbody>
    </table>
<?php endif; ?>
    <table class="form-table">
        <tbody>
			<tr>
                <th scope="row">
                    <label for="cf_geo_enable_banner"><?php echo __('Enable Country Flags',WP_CF_GEO_PLUGIN_NAME); ?>:</label>
                </th>
                <td>
                    <select name="cf_geo_enable_flag" id="cf_geo_enable_flag"<?php echo $enableForm; ?>>
                        <option value="true"<?php echo (get_option("cf_geo_enable_flag")=="true"?' selected':''); ?>><?php echo __('YES',WP_CF_GEO_PLUGIN_NAME); ?></option>
                        <option value="false"<?php echo (get_option("cf_geo_enable_flag")!="true"?' selected':''); ?>><?php echo __('NO',WP_CF_GEO_PLUGIN_NAME); ?></option>
                    </select>
                    <p><?php echo __("Enable/Disable Geo Banner.",WP_CF_GEO_PLUGIN_NAME); ?></p>
                </td>
			</tr>
			<tr>
				<th scope="row">
                    <label for="cf_geo_enable_banner"><?php echo __('Enable SEO Redirection',WP_CF_GEO_PLUGIN_NAME); ?>:</label>
                </th>
				<td>
                    <select name="cf_geo_enable_seo_redirection" id="cf_geo_enable_seo_redirection"<?php echo $enableForm; ?>>
                        <option value="true"<?php echo (get_option("cf_geo_enable_seo_redirection")=="true"?' selected':''); ?>><?php echo __('YES',WP_CF_GEO_PLUGIN_NAME); ?></option>
                        <option value="false"<?php echo (get_option("cf_geo_enable_seo_redirection")!="true"?' selected':''); ?>><?php echo __('NO',WP_CF_GEO_PLUGIN_NAME); ?></option>
                    </select>
                    <p><?php echo __("Enable/Disable Geo Banner.",WP_CF_GEO_PLUGIN_NAME); ?></p>
                </td>
            </tr>
        	<tr>
                <th scope="row">
                    <label for="cf_geo_enable_banner"><?php echo __('Enable Geo Banner',WP_CF_GEO_PLUGIN_NAME); ?>:</label>
                </th>
                <td>
                    <select name="cf_geo_enable_banner" id="cf_geo_enable_banner"<?php echo $enableForm; ?>>
                        <option value="true"<?php echo (get_option("cf_geo_enable_banner")=="true"?' selected':''); ?>><?php echo __('YES',WP_CF_GEO_PLUGIN_NAME); ?></option>
                        <option value="false"<?php echo (get_option("cf_geo_enable_banner")!="true"?' selected':''); ?>><?php echo __('NO',WP_CF_GEO_PLUGIN_NAME); ?></option>
                    </select>
                    <p><?php echo __("Enable/Disable Geo Banner.",WP_CF_GEO_PLUGIN_NAME); ?></p>
                </td>
            </tr>
            <tr>
                <th scope="row">
                    <label for="cf_geo_enable_gmap"><?php echo __('Enable Google Map',WP_CF_GEO_PLUGIN_NAME); ?>:</label>
                </th>
                <td>
                    <select name="cf_geo_enable_gmap" id="cf_geo_enable_gmap">
                        <option value="true"<?php echo (get_option("cf_geo_enable_gmap")=="true"?' selected':''); ?>><?php echo __('YES',WP_CF_GEO_PLUGIN_NAME); ?></option>
                        <option value="false"<?php echo (get_option("cf_geo_enable_gmap")!="true"?' selected':''); ?>><?php echo __('NO',WP_CF_GEO_PLUGIN_NAME); ?></option>
                    </select>
                    <p><?php echo __("Enable/Disable Google Map.",WP_CF_GEO_PLUGIN_NAME); ?></p>
                </td>
            </tr>
            <tr>
                <th scope="row">
                    <label for="cf_geo_enable_defender"><?php echo __('Enable Geo Defender',WP_CF_GEO_PLUGIN_NAME); ?>:</label>
                </th>
                <td>
                    <select name="cf_geo_enable_defender" id="cf_geo_enable_defender">
                        <option value="true"<?php echo (get_option("cf_geo_enable_defender")=="true"?' selected':''); ?>><?php echo __('YES',WP_CF_GEO_PLUGIN_NAME); ?></option>
                        <option value="false"<?php echo (get_option("cf_geo_enable_defender")!="true"?' selected':''); ?>><?php echo __('NO',WP_CF_GEO_PLUGIN_NAME); ?></option>
                    </select>
                    <p><?php echo __("Enable/Disable Geo Defender.",WP_CF_GEO_PLUGIN_NAME); ?></p>
                </td>
            </tr>
        	<tr>
                <th scope="row">
                    <label for="cf_geo_enable_cloudflare"><?php echo __('Enable Cloudflare',WP_CF_GEO_PLUGIN_NAME); ?>:</label>
                </th>
                <td>
                    <select name="cf_geo_enable_cloudflare" id="cf_geo_enable_cloudflare"<?php echo $enableForm; ?>>
                        <option value="true"<?php echo (get_option("cf_geo_enable_cloudflare")=="true"?' selected':''); ?>><?php echo __('YES',WP_CF_GEO_PLUGIN_NAME); ?></option>
                        <option value="false"<?php echo (get_option("cf_geo_enable_cloudflare")!="true"?' selected':''); ?>><?php echo __('NO',WP_CF_GEO_PLUGIN_NAME); ?></option>
                    </select>
                    <p><?php echo __("Enable/disable Cloudflare connection.",WP_CF_GEO_PLUGIN_NAME); ?></p>
                </td>
            </tr>
            <tr>
                <th scope="row">
                    <label for="cf_geo_enable_dns_lookup"><?php echo __('Enable DNS Lookup',WP_CF_GEO_PLUGIN_NAME); ?>:</label>
                </th>
                <td>
                    <select name="cf_geo_enable_dns_lookup" id="cf_geo_enable_dns_lookup"<?php echo $enableForm; ?>>
                        <option value="true"<?php echo (get_option("cf_geo_enable_dns_lookup")=="true"?' selected':''); ?>><?php echo __('YES',WP_CF_GEO_PLUGIN_NAME); ?></option>
                        <option value="false"<?php echo (get_option("cf_geo_enable_dns_lookup")!="true" ?' selected':''); ?>><?php echo __('NO',WP_CF_GEO_PLUGIN_NAME); ?></option>
                    </select>
                    <p><?php echo __("ATTENTION! Sometimes this can slowdown your WordPress blog.",WP_CF_GEO_PLUGIN_NAME); ?></p>
                </td>
            </tr>
            <tr>
                <th scope="row">
                    <label for="cf_geo_onyly_timezone"><?php echo __('Enable SSL',WP_CF_GEO_PLUGIN_NAME); ?>:</label>
                </th>
                <td>
                    <select name="cf_geo_enable_ssl" id="cf_geo_enable_ssl"<?php echo $enableForm; ?>>
                        <option value="true"<?php echo (get_option("cf_geo_enable_ssl")=="true"?' selected':''); ?>><?php echo __('YES',WP_CF_GEO_PLUGIN_NAME); ?></option>
                        <option value="false"<?php echo (get_option("cf_geo_enable_ssl")!="true"?' selected':''); ?>><?php echo __('NO',WP_CF_GEO_PLUGIN_NAME); ?></option>
                    </select>
                </td>
            </tr>
            <tr style="display:none">
                <th scope="row">
                    <label for="cf_geo_connection_timeout"><?php echo __('API Connection Timeout',WP_CF_GEO_PLUGIN_NAME); ?>:</label>
                </th>
                <td>
                    <input type="number" id="cf_geo_connection_timeout" name="cf_geo_connection_timeout" value="<?php echo (get_option("cf_geo_connection_timeout")>0?get_option("cf_geo_connection_timeout"):9); ?>" min="1" max="50"<?php echo $enableForm; ?>>
                    <p><?php echo __('Timeout for the API connection phase (Default is 9)',WP_CF_GEO_PLUGIN_NAME); ?></p>
                </td>
            </tr>
            <tr style="display:none">
                <th scope="row">
                    <label for="cf_geo_connection_timeout"><?php echo __('API Timeout',WP_CF_GEO_PLUGIN_NAME); ?>:</label>
                </th>
                <td>
                    <input type="number" id="cf_geo_connection_timeout" name="cf_geo_timeout" value="<?php echo (get_option("cf_geo_timeout")>0?get_option("cf_geo_timeout"):9); ?>" min="1" max="50"<?php echo $enableForm; ?>>
                    <p><?php echo __('Set maximum time of the API request what is allowed to take (Default is 9)',WP_CF_GEO_PLUGIN_NAME); ?></p>
                </td>
            </tr>
            <tr>
                <th scope="row">
                    <label for="cf_geo_enable_proxy"><?php echo __('Enable Proxy',WP_CF_GEO_PLUGIN_NAME); ?>:</label>
                </th>
                <td>
                    <select name="cf_geo_enable_proxy" id="cf_geo_enable_proxy"<?php echo $enableForm; ?>>
                        <option value="true"<?php echo (get_option("cf_geo_enable_proxy")=="true"?' selected':''); ?>><?php echo __('YES',WP_CF_GEO_PLUGIN_NAME); ?></option>
                        <option value="false"<?php echo (get_option("cf_geo_enable_proxy")!="true"?' selected':''); ?>><?php echo __('NO',WP_CF_GEO_PLUGIN_NAME); ?></option>
                    </select> IP:
                    <input name="cf_geo_enable_proxy_ip" type="text" value="<?php echo get_option("cf_geo_enable_proxy_ip"); ?>"<?php echo $enableForm; ?> style="width:180px" max="45" maxlength="45" disabled>
                     PORT:<input name="cf_geo_enable_proxy_port" type="text" value="<?php echo get_option("cf_geo_enable_proxy_port"); ?>"<?php echo $enableForm; ?> style="width:60px" max="6" maxlength="6" disabled> - <a href="http://nordvpn.com/?ref=18279552" target="_blank"><?php echo __('Need Proxy? We have Recommended Service For You.',WP_CF_GEO_PLUGIN_NAME); ?></a><br><br>
                     Username <input name="cf_geo_enable_proxy_username" type="text" value="<?php echo get_option("cf_geo_enable_proxy_username"); ?>"<?php echo $enableForm; ?> style="width:180px" max="45" maxlength="45" disabled>
                     Password <input name="cf_geo_enable_proxy_password" type="password" value="<?php echo get_option("cf_geo_enable_proxy_password"); ?>"<?php echo $enableForm; ?> style="width:180px" max="45" maxlength="45" disabled>
                     <div class="manage-menus">
                        <strong><?php echo __('NOTE & INFO',WP_CF_GEO_PLUGIN_NAME); ?></strong><br><br><?php echo __('Some servers not share real IP because of security reasons or IP is blocked from geolocation. Using proxy you can bypass that protocols and enable geoplugin to work properly. Also, this option on individual servers can cause inaccurate geo informations, and because of that this option is disabled by default. You need to test this option on your side and use.',WP_CF_GEO_PLUGIN_NAME); ?>
                     </div>
                </td>
            </tr>
        </tbody>
    </table>
</form>