<?php
	$defender = new CF_Geoplugin_Defender;
	$enable=$defender->enable;
?>
<div class="wrap" id="admin-page-geoplugin-new">
	<div class="welcome-panel">
		<h1><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span> <?php echo __('NEW Version', WP_CF_GEO_PLUGIN_NAME); ?> <?php echo WP_CF_GEO_PLUGIN_VERSION; ?> <span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></h1>

		<h3><?php echo __('Welcome to version', WP_CF_GEO_PLUGIN_NAME); ?> <?php echo WP_CF_GEO_PLUGIN_VERSION; ?>!<h3>
		<p><?php echo __('Over the past year, we develop this plugin to be useful to you and from time to time we add new options according to your requirements and our research according to what is now most necessary for geolocation marketing strategy. Therefore, we introduced a new necessary and useful features in one place and have made a significant step in the development of our plugin.', WP_CF_GEO_PLUGIN_NAME); ?></p>
		<ul>
			<li class="title"><h3><?php echo __("PRO Features:",WP_CF_GEO_PLUGIN_NAME); ?></h3></li>
			<li><span class="fa fa-check" aria-hidden="true"></span> <?php echo __("Cloudflare Geolocation Support",WP_CF_GEO_PLUGIN_NAME); ?></li>
			<li><span class="fa fa-check" aria-hidden="true"></span> <?php echo __("Proxy Settings",WP_CF_GEO_PLUGIN_NAME); ?></li>
			<li><span class="fa fa-check" aria-hidden="true"></span> <?php echo __("DNS Lookup",WP_CF_GEO_PLUGIN_NAME); ?></li>
			<li><span class="fa fa-check" aria-hidden="true"></span> <?php echo __("SSL",WP_CF_GEO_PLUGIN_NAME); ?></li>
			<li><span class="fa fa-check" aria-hidden="true"></span> <?php echo __("IP Version Lookup",WP_CF_GEO_PLUGIN_NAME); ?></li>
			<li><span class="fa fa-check" aria-hidden="true"></span> <?php echo __("CF Geo Banner",WP_CF_GEO_PLUGIN_NAME); ?></li>
			<li><span class="fa fa-check" aria-hidden="true"></span> <?php echo __("Country SEO Redirection",WP_CF_GEO_PLUGIN_NAME); ?></li>
			<li><span class="fa fa-check" aria-hidden="true"></span> <?php echo __("Country Flag Support",WP_CF_GEO_PLUGIN_NAME); ?></li>
			<li><span class="fa fa-check" aria-hidden="true"></span> <?php echo __("Google Map Global Settings",WP_CF_GEO_PLUGIN_NAME); ?></li>
			<li><span class="fa fa-check" aria-hidden="true"></span> <?php echo __("CF Geo Defender Full Functionality",WP_CF_GEO_PLUGIN_NAME); ?></li>
			<li><span class="fa fa-check" aria-hidden="true"></span> <?php echo __("Include/Exclude by Geolocation Functionality",WP_CF_GEO_PLUGIN_NAME); ?></li>
			<li><span class="fa fa-check" aria-hidden="true"></span> <?php echo __("Lifetime Lycense, Support & Updaes",WP_CF_GEO_PLUGIN_NAME); ?></li>
		</ul>
		<?php if($enable===false): ?>
			<h3><?php printf(__('To register your PRO version, you need to visit %s', WP_CF_GEO_PLUGIN_NAME), '<a href="'.get_admin_url().'/admin.php?page=cf-geoplugin-settings">'.__('Settings page', WP_CF_GEO_PLUGIN_NAME).'</a>'); ?></h3>
		<?php else: ?>
			<h3><?php echo __('Feel free to use this plugin with no limitation!', WP_CF_GEO_PLUGIN_NAME); ?></h3>
			<h3><?php printf(__('Continue with %s', WP_CF_GEO_PLUGIN_NAME), '<a href="'.get_admin_url().'/admin.php?page=cf-geoplugin">'.__('CF GeoPlugin', WP_CF_GEO_PLUGIN_NAME).'</a>'); ?></h3>
		<?php endif; ?>
		<br>
	</div>
</div>