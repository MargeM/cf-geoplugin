<?php
/**
 * @link              http://cfgeoplugin.com/
 * @since             1.0.0
 * @package           CF_Geoplugin
 *
 * @wordpress-plugin
 * Plugin Name:       CF Geo Plugin
 * Plugin URI:        http://cfgeoplugin.com/
 * Description:       Create Dynamic Content, Banners and Images on Your Website Based On Visitor Geo Location By Using Shortcodes With CF GeoPlugin.
 * Version:           5.6.1
 * Author:            Ivijan-Stefan Stipic
 * Author URI:        https://linkedin.com/in/ivijanstefanstipic
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       cf-geoplugin
 * Domain Path:       /languages
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) || ! defined( 'ABSPATH' ) ) die( "Don't mess with us." );

// Define PHP version for older servers
if(!defined('PHP_VERSION'))
{
	define('PHP_VERSION', phpversion());
}
if (!defined('PHP_VERSION_ID')) {
	$version = explode('.', PHP_VERSION);
	define('PHP_VERSION_ID', (($version[0] * 10000) + ($version[1] * 100) + $version[2]));
}
if (PHP_VERSION_ID < 50207) {
	if(!(isset($version))) $version = explode('.', PHP_VERSION);
	if(!defined('PHP_MAJOR_VERSION'))   define('PHP_MAJOR_VERSION',   $version[0]);
	if(!defined('PHP_MINOR_VERSION'))   define('PHP_MINOR_VERSION',   $version[1]);
	if(!defined('PHP_RELEASE_VERSION')) define('PHP_RELEASE_VERSION', $version[2]);
	
}

// Debug mode enabled
if ( defined( 'WP_CF_GEO_DEBUG' ) ){
	if(WP_CF_GEO_DEBUG === true || WP_CF_GEO_DEBUG === 1)
	{
		error_reporting( E_ALL );
		if(function_exists('ini_set'))
		{
			ini_set('display_startup_errors',1);
			ini_set('display_errors',1);
		}
	}
}
if ( ! defined( 'WP_CF_GEO_PLUGIN_FILE' ) ) define( 'WP_CF_GEO_PLUGIN_FILE', __FILE__ );
if ( ! defined( 'WP_CF_GEO_PLUGIN_ROOT' ) ) define( 'WP_CF_GEO_PLUGIN_ROOT', __DIR__ );
if ( ! defined( 'WP_CF_GEO_PLUGIN_URL' ) ) define( 'WP_CF_GEO_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
if ( ! defined( 'WP_CF_GEO_PLUGIN_VERSION' ) ) define('WP_CF_GEO_PLUGIN_VERSION', '5.6.1');
if ( ! defined( 'WP_CF_GEO_PLUGIN_NAME' ) ) define('WP_CF_GEO_PLUGIN_NAME', 'cf-geoplugin');
if ( ! defined( 'WP_CF_GEO_PLUGIN_METABOX' ) ) define('WP_CF_GEO_PLUGIN_METABOX', 'cf_geo_metabox_');
if ( ! defined( 'WP_CF_GEO_PLUGIN_PREFIX' ) ) define('WP_CF_GEO_PLUGIN_PREFIX', 'cf_geo_'.preg_replace("/[^0-9]/Ui",'',WP_CF_GEO_PLUGIN_VERSION).'_');
if ( ! defined( 'WP_CF_GEO_PLUGIN_PREMIUM_PRICE' ) ) define('WP_CF_GEO_PLUGIN_PREMIUM_PRICE', '32');
if ( ! defined( 'PHP_VERSION' ) ) define('PHP_VERSION', phpversion());

// Check SSL
if(!function_exists('cf_geo_is_ssl')) {
	function cf_geo_is_ssl($url = false)
    {
		if($url !== false && is_string($url)) {
			return (preg_match('/(https|ftps)/Ui', $url) !== false);
		} else if( is_admin() && defined('FORCE_SSL_ADMIN') && FORCE_SSL_ADMIN ===true ) {
			return true;
		} else if( (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ||
			(isset($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on') ||
			(isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') ||
			(isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == 443) )
		{
			return true;
		}
		return false;
    }
}

/**
 * Start sessions if not exists
 *
 * @author     Ivijan-Stefan Stipic <creativform@gmail.com>
 */
if (version_compare(PHP_VERSION, '7.0.0') >= 0) {
    if(session_status() == PHP_SESSION_NONE) {
        session_start(array(
          'cache_limiter' => 'private_no_expire',
          'read_and_close' => false,
       ));
    }
}
else if (version_compare(PHP_VERSION, '5.4.0') >= 0)
{
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
}
else
{
    if(session_id() == '') {
        session_start();
    }
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-cf-geoplugin-activator.php
 */
function activate_cf_geoplugin() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-cf-geoplugin-activator.php';
	CF_Geoplugin_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-cf-geoplugin-deactivator.php
 */
function deactivate_cf_geoplugin() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-cf-geoplugin-deactivator.php';
	CF_Geoplugin_Deactivator::deactivate();
}

/**
* Get Custom Post Data from forms
* @autor    Ivijan-Stefan Stipic
* @since    5.0.0
* @version  1.0.1
**/
if ( ! function_exists( 'CF_Geoplugin_Metabox_GET' ) ) :
	function CF_Geoplugin_Metabox_GET($name, $id=false, $single=false){
		global $post_type, $post;
		
		$name=trim($name);
		$prefix=WP_CF_GEO_PLUGIN_METABOX;
		$data=NULL;
	
		if($id!==false && !empty($id) && $id > 0)
			$getMeta=get_post_meta((int)$id, $prefix.$name, $single);
		else if(NULL!==get_the_id() && false!==get_the_id() && get_the_id() > 0)
			$getMeta=get_post_meta(get_the_id(),$prefix.$name, $single);
		else if(isset($post->ID) && $post->ID > 0)
			$getMeta=get_post_meta($post->ID,$prefix.$name, $single);
		else if('page' == get_option( 'show_on_front' ))
			$getMeta=get_post_meta(get_option( 'page_for_posts' ),$prefix.$name, $single);
		else if(is_home() || is_front_page() || get_queried_object_id() > 0)
			$getMeta=get_post_meta(get_queried_object_id(),$prefix.$name, $single);
		else
			$getMeta=false;
		
		return (!empty($getMeta)?$getMeta:NULL);
	}
endif;

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
if(!class_exists('CF_Geoplugin'))
{
	require plugin_dir_path( __FILE__ ) . 'includes/class-cf-geoplugin.php';
}
/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    4.0.0
 */
function run_cf_geoplugin() {
	if(class_exists('CF_Geoplugin'))
	{
		$plugin = new CF_Geoplugin();
		$plugin->run();
		// Global Variable
		$GLOBALS['CF_Geo'] = $GLOBALS['CF_GEO'] = $plugin->get();
	}
}
run_cf_geoplugin();

register_activation_hook( __FILE__, 'activate_cf_geoplugin' );
register_deactivation_hook( __FILE__, 'deactivate_cf_geoplugin' );