<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link      http://cfgeoplugin.com/
 * @since      4.2.1
 *
 * @package    CF_Geoplugin
 * @subpackage CF_Geoplugin/includes
 */

/**
 * Register all metabox inside posts.
 *
 * @package    CF_Geoplugin
 * @subpackage CF_Geoplugin/includes
 * @author     Ivijan-Stefan Stipic <creativform@gmail.com>
 */
 class CF_Geoplugin_Metabox
{
 
    /**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    4.0.0
	 * @access   protected
	 * @var      CF_Geoplugin_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    4.0.0
	 * @access   protected
	 * @var      string    $cf_geoplugin    The string used to uniquely identify this plugin.
	 */
	protected $cf_geoplugin;
	
	/**
	 * The unique prefix of this plugin.
	 *
	 * @since    4.0.0
	 * @access   protected
	 * @var      string    $prefix
	 */
	protected $prefix;
	protected $metabox;
	protected $defender=false;
	
	
	function __construct(){
		if ( is_admin() )
		{
			$this->cf_geoplugin	= WP_CF_GEO_PLUGIN_NAME;
			$this->version 		= WP_CF_GEO_PLUGIN_VERSION;
			$this->prefix	 	= WP_CF_GEO_PLUGIN_PREFIX;
			$this->metabox		= WP_CF_GEO_PLUGIN_METABOX;
			
			$encrypt = new CF_Geoplugin_Defender;
			$this->defender = $encrypt->enable;
			
            add_action( 'init', array($this, 'init_metabox'), 9999 );
			add_filter( 'cfgeo_meta_boxes', array($this, 'metaboxes') );
        }
		else
		{
			add_action('template_redirect', array($this, 'redirect'));
		}
 
    }
	public function metaboxes(array $meta_boxes){
		$prefix = $this->metabox;
		$meta_boxes=array();
		
		
		$all_countries = cf_geo_get_terms(array(
			'taxonomy'		=> 'cf-geoplugin-country',
			'hide_empty'	=> false
		));
		$countries=array();
		$countries[]=array(
			'value'	=>	'',
			'name'	=>	__('Choose country...',$this->cf_geoplugin),
		);
		if(is_array( $all_countries ) && count($all_countries)>0)
		{
			//$cf_geo_block_country = CF_Geoplugin_Metabox_GET('country', $post->ID, true);
			//$find_current = array_map("trim",explode(",",$cf_geo_block_country));
			foreach($all_countries as $i=>$country)
			{
				$countries[]=array(
					'value'	=>	$country->slug,
					'name'	=>	$country->name. ' - ' .$country->description,
				);
			}
		}
		
		$http = array();
		
		
		
		foreach(array(
		301 => __('Moved Permanently', $this->cf_geoplugin),
		302 => __('Moved Temporary', $this->cf_geoplugin),
		303 => __('See Other', $this->cf_geoplugin),
		404 => __('Not Found (not recommended)', $this->cf_geoplugin),
		) as $status_code=>$status_message)
		{
			$http[]=array(
				'value'	=>	$status_code,
				'name'	=>	$status_code. ' ' .$status_message,
			);
		}
		
		$meta_boxes[] = array(
			'id'         => 'cf_geoplugin_seo_redirection',
			'title'      => __('Country SEO Redirection',$this->cf_geoplugin),
		//	'pages'      => array('post', 'page', 'dashboard', 'attachment', 'archive', 'product', 'product_cat', 'product_tag', 'product_variation', 'shop_order', 'shop_order_status', 'shop_order_refund', 'shop_coupon', 'shop_webhook', 'blog'),
			//'pages'      => array('post', 'page', 'attachment', 'archive', 'product', 'product_cat', 'product_tag', 'product_variation', 'shop_order', 'shop_order_status', 'shop_order_refund', 'shop_coupon', 'shop_webhook', 'blog'),
			'pages'      => get_post_types(),
			'context'    => 'side',
			'priority'   => 'high',
			'show_names' => true,
			'desc' => __( 'Here you can safe redirect this page to another page using geolocation.', $this->cf_geoplugin ),
			//'show_on'    => array( 'key' => 'id', 'value' => array( 2, ), ), // Specific post IDs to display this metabox
			'fields' => array(
				array(
					'name' => __('Select Country',$this->cf_geoplugin),
					'desc' => __('Select the country you want to redirect.',$this->cf_geoplugin),
					'default' => '',
					'id' => $prefix . 'country',
					'type' => 'select',
					'options'=>$countries,
				),
				array(
					'name' => __('Redirect URL',$this->cf_geoplugin),
					'desc' => __('URL where you want to redirect',$this->cf_geoplugin),
					'default' => '',
					'id' => $prefix . 'url',
					'type' => 'text'
				),
				array(
					'name' => __('HTTP Code',$this->cf_geoplugin),
					'desc' => __('Select the desired HTTP redirection (HTTP CODE 302 is recommended)',$this->cf_geoplugin),
					'default' => 302,
					'id' => $prefix . 'status_code',
					'type' => 'select',
					'options'=>$http,
				),
				array(
					'name'    => __('Enable SEO Redirection',$this->cf_geoplugin),
					'id'      => $prefix . 'enable_seo',
					'type'    => 'radio_inline',
					'default' => 'false',
					'desc'	  => ($this->defender===false?__('CF GeoPlugin SEO Redirection is only enabled in PRO version.',$this->cf_geoplugin):''),
					'options' => array(
						'true' => __( 'Enabled', $this->cf_geoplugin ),
						'false'   => __( 'Disabled', $this->cf_geoplugin )
					),
				),
			)
		);
		
		return $meta_boxes;
	}
	
	/**
     * Meta box initialization.
     */
    public function init_metabox() {
        if ( ! class_exists( 'cfGeo_Meta_Box' ) )
			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/metabox/init.php';
    }
	
	public function redirect(){
		$response = array(
			'geo' => CF_Geoplugin_Metabox_GET('country', false, true),
			'url' => CF_Geoplugin_Metabox_GET('url', false, true),
			'status_code' => CF_Geoplugin_Metabox_GET('status_code', false, true),
		);
		
		$find_current = array_map("strtolower",array_map("trim",explode(",",$response['geo'])));
		
		if(
			in_array($response['status_code'], array(301,302,303,404)) 
			&& (filter_var($response['url'], FILTER_VALIDATE_URL) !== false)
			&& in_array(strtolower(do_shortcode('[cf_geo return="country_code"]')), $find_current)
		)
		{
			wp_redirect($response['url'], $response['status_code']);
			exit;
		}
	}
	
	private function load_banner_taxonomy(){
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-cf-geoplugin-data.php';
		$install = new CF_Geoplugin_Data();
		
		$install->install_country_terms("country-list","cf-geoplugin-country");
	}
}