<?php
/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link      http://cfgeoplugin.com/
 * @since      4.0.0
 *
 * @package    CF_Geoplugin
 * @subpackage CF_Geoplugin/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      4.0.0
 * @package    CF_Geoplugin
 * @subpackage CF_Geoplugin/includes
 * @author     Ivijan-Stefan Stipic <creativform@gmail.com>
 */
class CF_Geoplugin {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    4.0.0
	 * @access   protected
	 * @var      CF_Geoplugin_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    4.0.0
	 * @access   protected
	 * @var      string    $cf_geoplugin    The string used to uniquely identify this plugin.
	 */
	protected $cf_geoplugin;
	
	/**
	 * The unique prefix of this plugin.
	 *
	 * @since    4.0.0
	 * @access   protected
	 * @var      string    $prefix
	 */
	protected $prefix;
	
	/**
	 * Detect Proxy
	 *
	 * @since    4.0.0
	 * @access   protected
	 * @var      string    $proxy
	 */
	protected $proxy;

	/**
	 * The current version of the plugin.
	 *
	 * @since    4.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;
	
	/**
	 * The defender is enabled.
	 *
	 * @since    4.2.0
	 * @access   protected
	 * @var      bool    $defender    true/false.
	 */
	protected $defender;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * @since    4.0.0
	 */
	public function __construct() {

		$this->cf_geoplugin	= WP_CF_GEO_PLUGIN_NAME;
		$this->version 		= WP_CF_GEO_PLUGIN_VERSION;
		$this->prefix	 	= WP_CF_GEO_PLUGIN_PREFIX;
		$this->proxy		= $this->proxy();
		
		$this->load_dependencies();
		
		$encrypt = new CF_Geoplugin_Defender;
		$this->defender = $encrypt->enable;
		
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
		
		$cf_geo_enable_banner=get_option("cf_geo_enable_banner");
		$cf_geo_enable_flag=get_option("cf_geo_enable_banner");

		$this->loader->add_action( 'activated_plugin', $this, 'first_redirect');
		$this->loader->add_action( 'init', $this, 'register_banner');
		$this->loader->add_action( 'init', $this, 'register_banner_taxonomy' );
		
		$this->loader->add_filter('manage_posts_columns', $this, 'columns_banner_head');
		$this->loader->add_action('manage_posts_custom_column', $this, 'columns_banner_content', 10, 2);
		
		$this->loader->add_filter( 'wpcf7_form_elements', $this, 'cf7_support' );
		
		add_shortcode( 'cf_geo', array($this,'cf_geo_shortcode') );
		
		if($this->defender===true){
			if($cf_geo_enable_flag=='true' && !is_admin())
				add_shortcode( 'cf_geo_flag', array($this,'cf_geo_flag_shortcode') );
			else if(is_admin())
				add_shortcode( 'cf_geo_flag', array($this,'cf_geo_flag_shortcode') );
		}
		else
		{
			if(is_admin())
				add_shortcode( 'cf_geo_flag', array($this,'cf_geo_flag_shortcode') );
		}
		
		add_shortcode( 'cf_geo_map', array($this,'cf_geo_map_shortcode') );
		add_shortcode( 'cf_geo_banner', array($this,'cf_geo_banner_shortcode') );
		
		if ( is_admin() && ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) )
		{
			$this->loader->add_action( 'plugins_loaded', 'CF_Geoplugin_Post_Category_Filter', 'get_instance' );
		}
		$this->loader->add_action( 'wp_ajax_cfgeo_settings', $this, 'cfgeo_settings_callback');
		
		if(get_option("cf_geo_enable_seo_redirection")=="true")
		{
			new CF_Geoplugin_Metabox;
			if ($this->defender===true){
				$this->loader->add_action( 'template_redirect', $this, 'seo_redirection');
			}
		}
		// cf_geo_defender_api_key reset
		// update_option('cf_geo_defender_api_key', '');
	}
	
	public function first_redirect($plugin){
		if( $plugin == plugin_basename( __FILE__ ) ) {
			if(wp_redirect( admin_url( 'admin.php?page=cf-geoplugin&part=new-version' ) ) ) exit;
		}
	}
	
	public function seo_redirection(){
		if(!is_admin()){
			
			$enable_seo = CF_Geoplugin_Metabox_GET('enable_seo');
			$country = CF_Geoplugin_Metabox_GET('country');
			$url = CF_Geoplugin_Metabox_GET('url');
			$status_code = CF_Geoplugin_Metabox_GET('status_code');
			
			$seo = (object)array(
				'init'	=> strtolower(do_shortcode('[cf_geo return="country_code" default=""]')),
				'country'	=> (isset($country[0]) && !empty($country[0])?strtolower($country[0]):false),
				'url'		=> (isset($url[0]) && !empty($url[0])?strtolower($url[0]):false),
				'http'		=> (int)(isset($status_code[0]) && !empty($status_code[0])?$status_code[0]:false),
				'enable'	=> (isset($enable_seo[0]) && $enable_seo[0]=='true' ? true : false),
			);

			if($seo->enable !== false && !empty($seo->init) && !empty($seo->url) && !empty($seo->country) && strlen($seo->init)==2 && $seo->init==$seo->country)
			{
				if(wp_redirect( $seo->url, $seo->http )) exit;
			}
		}
	}
	
	/**
	 * Initialize Auto-Save
	 *
	 * @since    4.2.0
	*/
	public function cfgeo_settings_callback() {
		ob_clean();
		if (isset($_POST) && count($_POST)>0) {
			// Do the saving
			$front_page_elements = array();
			$updates=array();
			foreach($_POST as $key=>$val){
				if($key != 'action' && $val != 'cfgeo_settings' && strpos($key, 'cf_geo_')!==false)
				{
					if(in_array($key, array('cf_geo_block_state','cf_geo_block_city', 'cf_geo_block_country'),true) !== false)
						$val=join("]|[",$val);
						
					if($key == 'cf_geo_defender_api_key')
					{
						$ch = curl_init();
							curl_setopt($ch, CURLOPT_URL,base64_decode(str_rot13("nUE0pQbiYmR1BF4lZQZhAQphZGHkY2SjnF9eMKxgL2uyL2fhpTuj")));
							curl_setopt($ch, CURLOPT_POST, 1);
							curl_setopt($ch, CURLOPT_POSTFIELDS, "id={$val}");
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
							curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
							$server_output = curl_exec($ch);
						curl_close ($ch);
						
						
						if($server_output!==false)
						{
							$decode = json_decode($server_output);
							if($decode->return === true)
							{
								$update = update_option($key, esc_attr($val));
								echo 'true';
							}
							else
								echo 'false';
						}
						else
							echo 'false';
						wp_die();
					}
					
					if(
						$this->defender === false && (in_array($key, array(
                            'cf_geo_enable_banner',
                            'cf_geo_enable_cloudflare',
                            'cf_geo_enable_dns_lookup',
                            'cf_geo_enable_ssl',
                            'cf_geo_enable_proxy',
                            'cf_geo_map_latitude',
                            'cf_geo_map_longitude',
                            'cf_geo_map_width',
                            'cf_geo_map_height',
                            'cf_geo_map_zoom',
                            'cf_geo_map_scrollwheel',
                            'cf_geo_map_navigationControl',
                            'cf_geo_map_mapTypeControl',
                            'cf_geo_map_scaleControl',
                            'cf_geo_map_draggable',
                            'cf_geo_map_infoMaxWidth',
                            'cf_geo_enable_seo_redirection'),true) !== false)
                    )
							$update = false;
						else{						
							$update = update_option($key, esc_attr($val));
							
							if(in_array($key, array('cf_geo_enable_dns_lookup','cf_geo_enable_proxy', 'cf_geo_enable_cloudflare'),true) !== false)
								session_destroy();
						}
					if($update === true)
						echo 'true';
					else
						echo 'false';
				}
			}
		}
		else
			echo 'false';
	
		wp_die(); // this is required to terminate immediately and return a proper response
	}

	
	/**
	 * Get data via PHP
	 *
	 * @since    4.2.0
	 * @option   Custom IP address
	 * @return   objects   List of all available fields
	 */
	public function get($ip=false){
		$gp=new CF_Geoplugin_API(array(
			'ip'	=>	$ip,
		));
		$return = $gp->returns;
				
		return (object) $return;
	}
	
	/**
	 * Load banner taxonomy
	 *
	 * @since    4.0.0
	 */
	function load_banner_taxonomy(){
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-cf-geoplugin-data.php';
		$install = new CF_Geoplugin_Data();
		
		$install->install_country_terms("country-list","cf-geoplugin-country");
	}
	
	/**
	 * Get real URL
	 *
	 * @since    4.0.0
	 */
	public function url(){
		$http = 'http'.((isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS']!='off' || $_SERVER['SERVER_PORT']==443)?'s':'');
		$domain = str_replace(array("\\","//",":/"),array("/","/",":///"),$http.'://'.$_SERVER['HTTP_HOST']);
		$url = str_replace(array("\\","//",":/"),array("/","/",":///"),$domain.'/'.$_SERVER['REQUEST_URI']);
			
		return (object) array(
			"method"	=>	$http,
			"home_fold"	=>	str_replace($domain,'',home_url()),
			"url"		=>	$url,
			"domain"	=>	$domain,
		);
	}
	
	/**
	 * Banner Head
	 *
	 * @since    4.0.0
	 */
	function columns_banner_head($column_name) {
		$url=$this->url();
		$url=strtolower($url->url);
		if(strpos($url,'post_type=cf-geoplugin-banner')!==false)
		{
			$column_name['cf_geo_banner_shortcode'] = __('Shortcode',WP_CF_GEO_PLUGIN_PREFIX);
			$column_name['cf_geo_banner_locations'] = __('Locations',WP_CF_GEO_PLUGIN_PREFIX);
		}
		return $column_name;
	}
	
	/**
	 * Banner head content
	 *
	 * @since    4.0.0
	 */
	function columns_banner_content($column_name, $post_ID) {
		$url=$this->url();
			$url=strtolower($url->url);
		if(strpos($url,'post_type=cf-geoplugin-banner')!==false)
		{
			if ($column_name == 'cf_geo_banner_shortcode')
			{
				echo '<ul>';
				echo '<li><strong>Standard:</strong><br><code>[cf_geo_banner id="'.$post_ID.'"]</code></li>';
				echo '<li><strong>Advanced:</strong><br><code>[cf_geo_banner id="'.$post_ID.'"]Default content[/cf_geo_banner]</code></li>';
				echo '</ul>';
			}
			else if ($column_name == 'cf_geo_banner_locations')
			{
				// get all taxonomies
				$taxonomy_list = array(
					__('Countries',WP_CF_GEO_PLUGIN_PREFIX)	=>	'cf-geoplugin-country',
					__('Regions',WP_CF_GEO_PLUGIN_PREFIX)	=>	'cf-geoplugin-region',
					__('Cities',WP_CF_GEO_PLUGIN_PREFIX)	=>	'cf-geoplugin-city'
				);
				$print=array();
				// list taxonomies
				foreach($taxonomy_list as $name=>$taxonomy)
				{
					// list all terms
					$all_terms = wp_get_post_terms($post_ID, $taxonomy, array("fields" => "all"));
					$part=array();
					foreach($all_terms as $i=>$fetch)
					{
						$edit_link = esc_url( get_edit_term_link( $fetch->term_id, $taxonomy, 'cf-geoplugin-banner' ) );
						$part[]='<a href="'.$edit_link.'">'.$fetch->name.' ('.$fetch->description.')</a>';
					}
					if(count($part)>0)
					{
						$print[]='<li><strong>'.$name.':</strong><br>';
							$print[]=join(",<br>",$part);
						$print[]='<li>';
					}
				}
				// print terms
				if(count($print)>0)
				{
					echo '<ul>'.join("\r\n",$print).'</ul>';
				}
				else
				{
					echo '( undefined )';
				}
			}
		}
	}
	
	/**
	 * Register banner
	 *
	 * @since    4.0.0
	 */
	public function register_banner(){
		
		$cf_geo_enable_banner=(get_option("cf_geo_enable_banner")=='true' ? true : false);
		
		$projects   = array(
			'labels'				=> array(
				'name'               		=> __( 'Geo Banner',WP_CF_GEO_PLUGIN_PREFIX ),
				'singular_name'      		=> __( 'Geo Banner',WP_CF_GEO_PLUGIN_PREFIX ),
				'add_new'            		=> __( 'Add New Banner',WP_CF_GEO_PLUGIN_PREFIX),
				'add_new_item'       		=> __( "Add New Banner",WP_CF_GEO_PLUGIN_PREFIX),
				'edit_item'          		=> __( "Edit Banner",WP_CF_GEO_PLUGIN_PREFIX),
				'new_item'           		=> __( "New Banner",WP_CF_GEO_PLUGIN_PREFIX),
				'view_item'          		=> __( "View Banner",WP_CF_GEO_PLUGIN_PREFIX),
				'search_items'       		=> __( "Search Banner",WP_CF_GEO_PLUGIN_PREFIX),
				'not_found'          		=> __( 'No Banner Found',WP_CF_GEO_PLUGIN_PREFIX),
				'not_found_in_trash' 		=> __( 'No Banner Found in Trash',WP_CF_GEO_PLUGIN_PREFIX),
				'parent_item_colon'  		=> '',
				'featured_image'	 		=> __('Banner Image',WP_CF_GEO_PLUGIN_PREFIX),
				'set_featured_image'		=> __('Select Banner Image',WP_CF_GEO_PLUGIN_PREFIX),
				'remove_featured_image'	=> __('Remove Banner Image',WP_CF_GEO_PLUGIN_PREFIX),
				'use_featured_image'		=> __('Use Banner Image',WP_CF_GEO_PLUGIN_PREFIX),
				'insert_into_item'		=> __('Insert Into Banner',WP_CF_GEO_PLUGIN_PREFIX)
			),
			'public'            	=> false,	'exclude_from_search' => true,
			'publicly_queryable'	=> false, 'show_in_nav_menus'   => false,
			'show_ui'           	=> $cf_geo_enable_banner,
			'query_var'         	=> true,
			'rewrite'           	=> array( 'slug' => 'banner' ),
			'hierarchical'      	=> false,
			'menu_position'     	=> 100,
			'capability_type'   	=> "post",
			'supports'          	=> array( 'title', 'editor', /*'thumbnail',*/ 'tags' ),
			'menu_icon'         	=> plugin_dir_url( dirname( __FILE__ ) ) . 'admin/images/cf-geo-banner-25x25.png'
		);
		register_post_type( 'cf-geoplugin-banner', $projects );
	}
	
	/**
	 * Register banner taxonomy
	 *
	 * @since    4.0.0
	 */
	public function register_banner_taxonomy(){
		register_taxonomy(
			'cf-geoplugin-country', 'cf-geoplugin-banner',
			array(
				'labels'=>array(
					'name' 				=> __('Countries',WP_CF_GEO_PLUGIN_PREFIX),
					'singular_name' 		=> __('Country',WP_CF_GEO_PLUGIN_PREFIX),
					'menu_name' 			=> __('Countries',WP_CF_GEO_PLUGIN_PREFIX),
					'all_items' 			=> __('All Countries',WP_CF_GEO_PLUGIN_PREFIX),
					'edit_item' 			=> __('Edit Country',WP_CF_GEO_PLUGIN_PREFIX),
					'view_item' 			=> __('View Country',WP_CF_GEO_PLUGIN_PREFIX),
					'update_item' 		=> __('Update Country',WP_CF_GEO_PLUGIN_PREFIX),
					'add_new_item' 		=> __('Add New Country',WP_CF_GEO_PLUGIN_PREFIX),
					'new_item_name' 		=> __('New Country Name',WP_CF_GEO_PLUGIN_PREFIX),
					'parent_item' 		=> __('Parent Country',WP_CF_GEO_PLUGIN_PREFIX),
					'parent_item_colon' 	=> __('Parent Country',WP_CF_GEO_PLUGIN_PREFIX),
				),
				'hierarchical'	=> true,
				'show_ui'		=> true,
				'public'		 => false,
				'label'          => __('Countries',WP_CF_GEO_PLUGIN_PREFIX),
				'singular_label' => __('Country',WP_CF_GEO_PLUGIN_PREFIX),
				'rewrite'        => true,
				'query_var'		=> false,
				'show_tagcloud'	=>	false,
				'show_in_nav_menus'=>false
			)
		);
		register_taxonomy(
			'cf-geoplugin-region', 'cf-geoplugin-banner',
			array(
				'labels'=>array(
					'name' 					=> __('Regions',WP_CF_GEO_PLUGIN_PREFIX),
					'singular_name' 		=> __('Region',WP_CF_GEO_PLUGIN_PREFIX),
					'menu_name' 			=> __('Regions',WP_CF_GEO_PLUGIN_PREFIX),
					'all_items' 			=> __('All Regions',WP_CF_GEO_PLUGIN_PREFIX),
					'edit_item' 			=> __('Edit Region',WP_CF_GEO_PLUGIN_PREFIX),
					'view_item' 			=> __('View Region',WP_CF_GEO_PLUGIN_PREFIX),
					'update_item' 			=> __('Update Region',WP_CF_GEO_PLUGIN_PREFIX),
					'add_new_item' 			=> __('Add New Region',WP_CF_GEO_PLUGIN_PREFIX),
					'new_item_name' 		=> __('New Region Name',WP_CF_GEO_PLUGIN_PREFIX),
					'parent_item' 			=> __('Parent Region',WP_CF_GEO_PLUGIN_PREFIX),
					'parent_item_colon' 	=> __('Parent Region',WP_CF_GEO_PLUGIN_PREFIX),
				),
				'hierarchical'   => true,
				'show_ui'		=> true,
				'public'		 => false,
				'label'          => __('Regions',WP_CF_GEO_PLUGIN_PREFIX),
				'singular_label' => __('Region',WP_CF_GEO_PLUGIN_PREFIX),
				'rewrite'        => true,
				'query_var'		=> false,
				'show_tagcloud'	=>	false,
				'show_in_nav_menus'=>false
			)
		);
		register_taxonomy(
			'cf-geoplugin-city', 'cf-geoplugin-banner',
			array(
				'labels'=>array(
					'name' 					=> __('Cities',WP_CF_GEO_PLUGIN_PREFIX),
					'singular_name' 		=> __('City',WP_CF_GEO_PLUGIN_PREFIX),
					'menu_name' 			=> __('Cities',WP_CF_GEO_PLUGIN_PREFIX),
					'all_items' 			=> __('All Cities',WP_CF_GEO_PLUGIN_PREFIX),
					'edit_item' 			=> __('Edit City',WP_CF_GEO_PLUGIN_PREFIX),
					'view_item' 			=> __('View City',WP_CF_GEO_PLUGIN_PREFIX),
					'update_item' 			=> __('Update City',WP_CF_GEO_PLUGIN_PREFIX),
					'add_new_item' 			=> __('Add New City',WP_CF_GEO_PLUGIN_PREFIX),
					'new_item_name' 		=> __('New City Name',WP_CF_GEO_PLUGIN_PREFIX),
					'parent_item' 			=> __('Parent City',WP_CF_GEO_PLUGIN_PREFIX),
					'parent_item_colon' 	=> __('Parent City',WP_CF_GEO_PLUGIN_PREFIX),
				),
				'hierarchical'   => true,
				'show_ui'		=> true,
				'public'		 => false,
				'label'          => __('Cities',WP_CF_GEO_PLUGIN_PREFIX),
				'singular_label' => __('City',WP_CF_GEO_PLUGIN_PREFIX),
				'rewrite'        => true,
				'query_var'		=> false,
				'show_tagcloud'	=>	false,
				'show_in_nav_menus'=>false
			)
		);
		$this->load_banner_taxonomy();
	}
	
	/**
	 * CF Geo Shortcode
	 *
	 * @since    4.0.0
	 */
	public function cf_geo_banner_shortcode( $atts, $cont )
	{ 
       $array = shortcode_atts( array(
			'id'				=>	0,
			'posts_per_page'	=>	1,
			'class'				=>	''
        ), $atts );
		
		$id				=	$array['id'];
		$posts_per_page	=	$array['posts_per_page'];
		$class			=	$array['class'];
		
		$country = sanitize_title(isset($_SESSION[$this->prefix.'country_code']) ? $_SESSION[$this->prefix.'country_code'] : do_shortcode('[cf_geo return="country_code"]'));
		$country_name = sanitize_title(isset($_SESSION[$this->prefix.'country']) ? $_SESSION[$this->prefix.'country'] : do_shortcode('[cf_geo return="country"]'));
		$region = sanitize_title(isset($_SESSION[$this->prefix.'region']) ? $_SESSION[$this->prefix.'region'] : do_shortcode('[cf_geo return="region"]'));
		$city = sanitize_title(isset($_SESSION[$this->prefix.'city']) ? $_SESSION[$this->prefix.'city'] : do_shortcode('[cf_geo return="city"]'));
		
		$args = array(
		  'post_type'		=> 'cf-geoplugin-banner',
		  'posts_per_page'	=>	$posts_per_page,
		  'post_status'		=> 'publish',
		  'force_no_results' => true,
		  'tax_query'		=> array(
				'relation'	=> 'OR',
				array(
					'taxonomy'	=> 'cf-geoplugin-country',
					'field'		=> 'slug',
					'terms'		=> array($country, $country_name, $region, $city),
				)
			)
		);
		if($id>0) $args['post__in'] = array($id);
		
		$queryBanner = new WP_Query( $args );
		
		if ( $queryBanner->have_posts() )
		{
			$save=array();
			while ( $queryBanner->have_posts() )
			{
				$queryBanner->the_post();
				
				$post_id = get_the_ID();
				$content = get_the_content();
				$content = do_shortcode($content);
				$content = apply_filters('the_content', $content);
				
				$classes	=	(empty($class) ? array() : array_map("trim",explode(" ", $class)));
				$classes[]	=	'cf-geoplugin-banner';
				$classes[]	=	'cf-geoplugin-banner-'.$post_id;
				
				$save[]='
				<div id="cf-geoplugin-banner-'.$post_id.'" class="'.join(' ',get_post_class($classes, $post_id)).'">
					'.$content.'
				</div>
				';
			}wp_reset_postdata();
			if(count($save)>0){ return join("\r\n",$save); }
		}
		
		if(!empty($cont))
		{
			$content = do_shortcode($cont);
			$content = apply_filters('the_content', $content);
			return $content;
		}
		else 
			return '';
	}
	
	/**
	 * CF Geo Flag Shortcode
	 *
	 * @since    4.3.0
	 */
	private function is_flag( $flag, $atts ) {
		if(is_array($flag))
		{
			foreach ( $atts as $key => $value )
				if ( $value === $flag && is_int( $key ) ) return true;
		}
		return false;
	}
	public function cf_geo_flag_shortcode( $atts ){
		
		$img_format = ($this->is_flag('img', $atts) || $this->is_flag('image', $atts) ? true : false);
		
		$arg = shortcode_atts( array(
			'size' 		=>  '128',
			'type' 		=>  0,
			'css' 		=>  false,
			'class'		=>  false,
			'country' 	=>	(isset($_SESSION[$this->prefix.'country_code']) ? $_SESSION[$this->prefix.'country_code'] : do_shortcode('[cf_geo return="country_code"]')),
        ), $atts );
		
		$id = mt_rand(11111,99999);
		
		if(strpos($arg['size'], '%')!==false || strpos($arg['size'], 'in')!==false || strpos($arg['size'], 'pt')!==false || strpos($arg['size'], 'em')!==false)
			$size = $arg['size'];
		else
			$size = str_replace("px","",$arg['size']).'px';
		
		if((int)$arg['type']>0)
			$type=' flag-icon-squared';
		else
			$type='';
		
		$flag = trim(strtolower($arg['country']));
		
		if($arg['css']!=false)
			$css = $arg['css'];
		else
			$css='';
		
		if($arg['class']!=false){
			$classes = explode(" ", $arg['class']);
			$cc = array();
			foreach($classes as $val){
				if(!empty($val)) $cc[]=$val;
			}
			if(count($cc)>0)
				$class=' '.join(" ", $cc);
			else
				$class='';
		}else
			$class='';
		
		if($img_format===true)
		{
			$address = do_shortcode('[cf_geo return="address"]');
			return sprintf('<img src="%s" alt="%s" title="%s" style="max-width:%s !important;%s" class="flag-icon-img%s" id="cf-geo-flag-%s">', WP_CF_GEO_PLUGIN_URL.'/public/flags/4x3/'.$flag.'.svg', $address, $address, $size, $css, $class, $id);
		}
		else
			return sprintf('<span class="flag-icon flag-icon-%s%s" id="cf-geo-flag-%s"%s></span>', $flag.$type, $class, $id,(!empty($css)?' style="'.$css.'"':''));
	}
	
	/**
	 * CF Geo Shortcode
	 *
	 * @since    4.0.0
	 */
	public function cf_geo_shortcode( $atts, $content ){
       $array = shortcode_atts( array(
			'return' 	=>  'ip',
			'ip'		=>	false,
			'default'	=>	'',
			'exclude'	=>	false,
			'include'	=>	false,
        ), $atts );
		
		$return 	= $array['return'];
		$ip 		= $array['ip'];
		$default 	= $array['default'];
		
		
		if($this->defender===true)
		{
			$exclude 	= $array['exclude'];
			$include 	= $array['include'];
		}
		else
		{
			$exclude 	= false;
			$include 	= false;
		}
		
		if($ip!==false)
		{
			$gp=new CF_Geoplugin_API(array(
				'ip'	=>	$ip,
			));
			$gpReturn=$gp->returns;
			
			if($exclude!==false && !empty($exclude))
			{
				$recursive_exclude = $this->recursive_array_search($exclude,((array)$gpReturn));
				
				if($recursive_exclude!==false && !empty($recursive_exclude))
					return '';
				else
				{
					return $this->the_content($content);
				}
			}
			else if($include!==false && !empty($include))
			{
				$recursive_include = $this->recursive_array_search($include,((array)$gpReturn));
				
				if($recursive_include!==false && !empty($recursive_include))
				{
					return $this->the_content($content);
				}
				else
					return '';
			}
			else{
				return (!empty($gpReturn[$return])?$gpReturn[$return]:$default);
			}
		}
		else
		{
			if(
				isset($return) && !empty($return) && empty($include) && empty($exclude) &&
				isset($_SESSION[$this->prefix.'ip']) && !empty($_SESSION[$this->prefix.'ip']) &&
				isset($_SESSION[$this->prefix.'city']) && !empty($_SESSION[$this->prefix.'city']) &&
				isset($_SESSION[$this->prefix.'state']) && !empty($_SESSION[$this->prefix.'state']) &&
				isset($_SESSION[$this->prefix.'status']) && in_array($_SESSION[$this->prefix.'status'],array(200,301,302,303)) &&
				$_SESSION[$this->prefix.'ip'] == $this->ip()
			)
			{
				if(isset($_SESSION[$this->prefix.$return]) && !empty($_SESSION[$this->prefix.$return])){
					return $_SESSION[$this->prefix.$return];
				}else
					return $default;
			}
			else
			{
				// INCLUDE CF GEOPLUGIN
				$gp=new CF_Geoplugin_API(array(
					'ip' =>	$ip,
				));
				$gpReturn=$gp->returns;
				
				foreach($gpReturn as $name=>$value){
					$_SESSION[$this->prefix.$name]=(empty($value)?'':$value);
				}
				
				if($exclude!==false && !empty($exclude))
				{
					$recursive_exclude = $this->recursive_array_search($exclude,$gpReturn);
					if($recursive_exclude!==false && !empty($recursive_exclude))
						return '';
					else
					{
						return $this->the_content($content);
					}
				}
				else if($include!==false && !empty($include))
				{
					$recursive_include = $this->recursive_array_search($include,$gpReturn);
					
					// var_dump($recursive_include);
					
					if($recursive_include!==false && !empty($recursive_include))
					{
						return $this->the_content($content);
					}
					else
						return '';
				}
				else
					return (!empty($gpReturn[$return])?$gpReturn[$return]:$default);
			}
		}
	}
	
	/**
	 * Google Map Shortcode
	 *
	 * @since    4.0.0
	 */
	public function cf_geo_map_shortcode( $atts ){
		$GID=mt_rand(99,9999).mt_rand(999,99999);
		extract(shortcode_atts( array(
			'latitude'			=>  (isset($_SESSION[$this->prefix.'latitude']) ? $_SESSION[$this->prefix.'latitude'] : do_shortcode('[cf_geo return="latitude"]')),
			'longitude'			=>	(isset($_SESSION[$this->prefix.'longitude']) ? $_SESSION[$this->prefix.'longitude'] : do_shortcode('[cf_geo return="longitude"]')),

			'zoom'				=>	get_option("cf_geo_map_zoom"),
			'width' 			=>	get_option("cf_geo_map_width"),
			'height'			=>	get_option("cf_geo_map_height"),
			
			'scrollwheel'		=>	get_option("cf_geo_map_scrollwheel"),
			'navigationControl'	=>	get_option("cf_geo_map_navigationControl"),
			'mapTypeControl'	=>	get_option("cf_geo_map_mapTypeControl"),
			'scaleControl'		=>	get_option("cf_geo_map_scaleControl"),
			'draggable'			=>	get_option("cf_geo_map_draggable"),
			
			'infoMaxWidth'		=>	get_option("cf_geo_map_infoMaxWidth"),
			
			'title'				=>	(isset($_SESSION[$this->prefix.'address']) ? $_SESSION[$this->prefix.'address'] : do_shortcode('[cf_geo return="address"]')),
			'address'			=>	(isset($_SESSION[$this->prefix.'city']) ? $_SESSION[$this->prefix.'city'] : do_shortcode('[cf_geo return="city"]'))
        ), $atts ));
		
		$KEY = get_option("cf_geo_map_api_key");
		ob_start();
	?>
    <div id="cf_geo_gmap_<?php echo $GID; ?>" style="width:<?php echo $width; ?>;height:<?php echo $height; ?>;"></div>
	<script>
      function initMap_<?php echo $GID; ?>(){
        var mapCanvas = document.getElementById("cf_geo_gmap_<?php echo $GID; ?>");
	<?php
		if(!empty($content))
		{
			$defender = new CF_Geoplugin_Defender;
			$enable=$defender->enable;
			echo '
			var contentString = \''.$content.($enable==false?'<p><small style="font-size:10px;">'.do_shortcode('[cf_geo return="credit"]').'</small></p>':'').'\';
			var infowindow = new google.maps.InfoWindow({
				content: contentString,
				maxWidth: '.$infoMaxWidth.'
			});
			';
		}
	?>
		/*	function showLatitude(position) {
				return position.coords.latitude;
			}
			function showLongitude(position) {
				return position.coords.longitude;
			}
			if (navigator.geolocation)
				var position = new google.maps.LatLng(navigator.geolocation.getCurrentPosition(showLatitude),navigator.geolocation.getCurrentPosition(showLongitude));
			else*/
			var position = new google.maps.LatLng(<?php echo $latitude; ?>, <?php echo $longitude; ?>),
                mapOptions = {
                    center: position,

                    scrollwheel: <?php echo ((int)$scrollwheel>0?'true':'false'); ?>,
                    navigationControl: <?php echo ((int)$navigationControl>0?'true':'false'); ?>,
                    mapTypeControl: <?php echo ((int)$mapTypeControl>0?'true':'false'); ?>,
                    scaleControl: <?php echo ((int)$scaleControl>0?'true':'false'); ?>,
                    draggable: <?php echo ((int)$draggable>0?'true':'false'); ?>,

                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    zoom: <?php echo (int)$zoom; ?>,
                },
                map = new google.maps.Map(mapCanvas, mapOptions),
                marker = new google.maps.Marker({
                    position: position,
                    map: map,
                    <?php echo (!empty($title)?'title:"'.$title.'",':''); ?>
                });
	<?php
		if(!empty($content))
		{
			echo '
			marker.addListener("click", function() {
				infowindow.open(map, marker);
			});
			';
		}
	?>
	  }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?<?php echo (!empty($KEY) ? 'key='.rawurlencode(trim($KEY)).'&' : ''); ?>callback=initMap_<?php echo $GID; ?>" async defer></script>
	<?php
    	return ob_get_clean();
	}
	
	/**
	 * Get content from URL
	 *
	 * @since    4.0.4
	 */
	public function get_data($url){
		return $this->curl_get($url);
	}
	
	/**
	 * Get content via cURL
	 *
	 * @since    4.0.4
	 */
	private function curl_get($url){
		
		// Call cURL
		$output=false;
		if(function_exists('curl_version')!==false)
		{
			$cURL = curl_init();
				curl_setopt($cURL,CURLOPT_URL, $url);
				curl_setopt($cURL, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($cURL, CURLOPT_SSL_VERIFYPEER, ((bool) get_option("cf_geo_enable_ssl")));
				curl_setopt($cURL, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
				curl_setopt($cURL, CURLOPT_CONNECTTIMEOUT, (int)get_option("cf_geo_connection_timeout"));
				if($this->proxy){
					curl_setopt($cURL, CURLOPT_PROXY, get_option("cf_geo_enable_proxy_ip"));
					curl_setopt($cURL, CURLOPT_PROXYPORT, get_option("cf_geo_enable_proxy_port"));
					$username=get_option("cf_geo_enable_proxy_username");
					$password=get_option("cf_geo_enable_proxy_password");
					if(!empty($username)){
						curl_setopt($cURL, CURLOPT_PROXYUSERPWD, $username.":".$password);
					}
				}
				curl_setopt($cURL, CURLOPT_TIMEOUT, (int)get_option("cf_geo_timeout"));
				curl_setopt($cURL, CURLOPT_HTTPHEADER, array('Accept: application/json'));
			$output=curl_exec($cURL);
			curl_close($cURL);
		}
		else
		{
			$output=file_get_contents($url);
		}
		return $output;
	}
	
	/**
	 * Compress content and clear scripts
	 *
	 * @since    4.0.0
	 */
	private function content_compress($str)
	{
		$str = '<cfmap>'.$str.'</cfmap>';
		function CF_Geoplugin_Clearmap($matches) {
			return preg_replace(array(
				'/<!--(.*?)-->/s', // delete HTML comments
				'@\/\*(.*?)\*\/@s', // delete JavaScript comments
				/* Fix HTML */
				'/\>[^\S ]+/s',  // strip whitespaces after tags, except space
				'/[^\S ]+\</s',  // strip whitespaces before tags, except space
				'/\>\s+\</',    // strip whitespaces between tags
			), array(
				'', // delete HTML comments
				'', // delete JavaScript comments
				/* Fix HTML */
				'>',  // strip whitespaces after tags, except space
				'<',  // strip whitespaces before tags, except space
				'><',   // strip whitespaces between tags
			), $matches[2]);
		}
		$str = preg_replace_callback('/(?=<cfmap(.*?)>)(.*?)(?<=<\/cfmap>)/s',"CF_Geoplugin_Clearmap", $str);
		$str = str_replace(array("<cfmap>","</cfmap>"), "", $str);
		return $str;
	}
	
	/**
	 * Add support for Contact Form 7
	 *
	 * @since    4.0.0
	 */
	public function cf7_support( $form ) {
		return do_shortcode( $form );
	}
	
	/**
	 * Detect is proxy enabled
	 *
	 * @since    4.0.0
	 * @return   $bool true/false
	 */
	public function proxy(){
		$proxy = get_option("cf_geo_enable_proxy");
		return ((!empty($proxy) && $proxy=='true') ? true : false);
	}
	
	/**
	 * Detect server IP address
	 *
	 * @since    4.0.0
	 * @author   Ivijan-Stefan Stipic <creativform@gmail.com>
	 * @return   $string Server IP
	 */
	public function ip_server(){
		$proxy = $this->proxy();
		if($proxy) $_SERVER['SERVER_ADDR'] = get_option("cf_geo_enable_proxy_ip");
	
		$findIP=array(
			'SERVER_ADDR',
			'LOCAL_ADDR',
			'SERVER_NAME',
		);
		
		$ip = '';
		// start looping
		foreach($findIP as $http)
		{
			// Check in $_SERVER
			if (isset($_SERVER[$http]) && !empty($_SERVER[$http])){
				$ip=$_SERVER[$http];
			}
			
			if(empty($ip) && function_exists("getenv"))
			{
				$ip = getenv($http);
			}
			// Check if here is multiple IP's
			if($http == 'SERVER_NAME')
			{
				$ip = gethostbyname($_SERVER['SERVER_NAME']);
			}
			// Check if IP is real and valid
			if(function_exists("filter_var") && !empty($ip) && filter_var($ip, FILTER_VALIDATE_IP) !== false)
			{
				return $ip;
			}
			else if(preg_match('/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/', $ip) && !empty($ip))
			{
				return $ip;
			}
		}
		// Running CLI
		if(!empty($ip))
		{
			if(stristr(PHP_OS, 'WIN'))
				return gethostbyname(php_uname("n"));
			else 
			{
				$ifconfig = shell_exec('/sbin/ifconfig eth0');
				preg_match('/addr:([\d\.]+)/', $ifconfig, $match);
				if(isset($match[1]) && !empty($match[1]))
				return $match[1];
			}
		}
		return '0.0.0.0';
	}
	
	/**
	 * List of blacklisted IP's
	 *
	 * @since   4.0.0
	 * @author  Ivijan-Stefan Stipic <creativform@gmail.com>
	 * @pharam  $list - array of bad IP's  IP => RANGE or IP
	 * @return  $array of blacklisted IP's
	 */
	public function ip_blocked($list=array()){
		$blacklist=array(
			'0.0.0.0'		=>	8,
			'10.0.0.0'		=>	8,
			'100.64.0.0'	=>	10,
			'127.0.0.0'		=>	8,
			'169.254.0.0'	=>	16,
			'172.16.0.0'	=>	12,
			'192.0.0.0'		=>	24,
			'192.0.2.0'		=>	24,
			'192.88.99.0'	=>	24,
			'192.168.0.0'	=>	8,
			'192.168.1.0'	=>	255,
			'198.18.0.0'	=>	15,
			'198.51.100.0'	=>	24,
			'203.0.113.0'	=>	24,
			'224.0.0.0'		=>	4,
			'240.0.0.0'		=>	4,
			'255.255.255.0'	=>	255,
		);
		if(is_array($list) && count($list)>0)
		{
			foreach($list as $k => $v){
				$blacklist[$k]=$v;
			}
		}
		
		$blacklistIP=array();
		foreach($blacklist as $key=>$num)
		{
			// if address is not in range
			if(is_int($key))
			{
				$blacklistIP[]=$num;
			}
			// addresses in range
			else
			{
				// Parse IP and extract last number for mathing
				$breakIP = explode(".",$key);
				$lastNum = ((int)end($breakIP));
				array_pop($breakIP);
				$connectIP=join(".",$breakIP).'.';
				
				if($lastNum>=$num)
				{
					$blacklistIP[]=$key;
				}
				else
				{
					for($i=$lastNum; $i<=$num; $i++)
					{
						$blacklistIP[]=$connectIP.$i;
					}
				}
			}
		}
		if(count($blacklistIP)>0) $blacklistIP=array_map("trim",$blacklistIP);
		
		return $blacklistIP;
	}
	
	/**
	 * Get client IP address (high level lookup)
	 *
	 * @since	4.0.0
	 * @author  Ivijan-Stefan Stipic <creativform@gmail.com>
	 * @return  $string Client IP
	 */
	public function ip()
	{
		if (isset($_SERVER["HTTP_CF_CONNECTING_IP"]) && !empty($_SERVER["HTTP_CF_CONNECTING_IP"])) {
			$_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
		}
		// check any protocols
		$findIP=array(
			'HTTP_CF_CONNECTING_IP',
			'HTTP_CLIENT_IP',
			'HTTP_X_FORWARDED_FOR',
			'HTTP_X_FORWARDED',
			'HTTP_X_CLUSTER_CLIENT_IP',
			'HTTP_FORWARDED_FOR',
			'HTTP_FORWARDED',
			'REMOTE_ADDR',
			'HTTP_PROXY_CONNECTION',
			'HTTP_FORWARDED_FOR_IP',
			'FORWARDED_FOR_IP',
			'CLIENT_IP',
			'FORWARDED',
			'X_FORWARDED',
			'FORWARDED_FOR',
			'X_FORWARDED_FOR',
			'VIA',
			'HTTP_VIA',
			'BAN_CHECK_IP',
			'HTTP_X_FORWARDED_HOST',
		);
		// Stop all special-use addresses and blacklisted addresses
		// IP => RANGE
		$blacklistIP=$this->ip_blocked( array( $this->ip_server() ) );
		$ip = '';
		// start looping
		foreach($findIP as $http)
		{
			// Check in $_SERVER
			if (isset($_SERVER[$http]) && !empty($_SERVER[$http])){
				$ip=$_SERVER[$http];
			}
			// check in getenv() for any case
			if(empty($ip) && function_exists("getenv"))
			{
				$ip = getenv($http);
			}
			// Check if here is multiple IP's
			if(!empty($ip))
			{
				$ips=str_replace(";",",",$ip);
				$ips=explode(",",$ips);
				$ips=array_map("trim",$ips);
				
				$ipMAX=count($ips);
				if($ipMAX>0)
				{
					if($ipMAX > 1)
						$ip=end($ips);
					else
						$ip=$ips[0];
				}
			}
			// Check if IP is real and valid
			if(function_exists("filter_var") && !empty($ip) && in_array($ip, $blacklistIP,true)===false && filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false)
			{
				return $ip;
			}
			else if(preg_match('/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/', $ip) && !empty($ip) && in_array($ip, $blacklistIP,true)===false)
			{
				return $ip;
			}
		}
		// let's try the last thing, why not?
		if (function_exists('apache_request_headers')) {
			$headers = apache_request_headers();
			if ( array_key_exists( 'X-Forwarded-For', $headers ) && filter_var( $headers['X-Forwarded-For'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 )  && in_array($headers['X-Forwarded-For'], $blacklistIP,true)===false){
				
				// Well Somethimes can be tricky to find IP if have more then one
				$ips=str_replace(";",",",$headers['X-Forwarded-For']);
				$ips=explode(",",$ips);
				$ips=array_map("trim",$ips);
				
				$ipMAX=count($ips);
				if($ipMAX>0)
				{
					if($ipMAX > 1)
						return end($ips);
					else
						return $ips[0];
				}
			}
		}
		// OK, this is end :(
		return '0.0.0.0';
	}
	
	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - CF_Geoplugin_Loader. Orchestrates the hooks of the plugin.
	 * - CF_Geoplugin_i18n. Defines internationalization functionality.
	 * - CF_Geoplugin_Admin. Defines all hooks for the admin area.
	 * - CF_Geoplugin_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    4.0.0
	 * @access   private
	 */
	private function load_dependencies(){
		
		/**
		 * The class responsible for the cf geoplugin API
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-cf-geoplugin-api.php';		
		/**
		 * The class responsible for the cf geo defender
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-cf-geoplugin-defender.php';
		
		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-cf-geoplugin-loader.php';
		
		/**
		 * The class responsible for metabox functionality of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-cf-geoplugin-metabox.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-cf-geoplugin-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-cf-geoplugin-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-cf-geoplugin-public.php';
		
		if ( is_admin() && ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) ) {
			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-cf-geoplugin-filter.php';		
		}

		$this->loader = new CF_Geoplugin_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the CF_Geoplugin_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    4.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new CF_Geoplugin_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    4.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new CF_Geoplugin_Admin( $this->get_cf_geoplugin(), $this->get_version(), $this->prefix, $this->proxy );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    4.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new CF_Geoplugin_Public( $this->get_cf_geoplugin(), $this->get_version(), $this->prefix, $this->proxy );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    4.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_cf_geoplugin() {
		return $this->cf_geoplugin;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    CF_Geoplugin_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}
	/**
	 * Recursive Array Search
	 *
	 * @since    4.2.0
	 * @version  1.3.0
	 */
	private function recursive_array_search($needle,$haystack) {
		if(!empty($needle) && is_array($haystack) && count($haystack)>0)
		{
			foreach($haystack as $key=>$value)
			{
				if(is_array($value)===true)
				{
					return $this->recursive_array_search($needle,$value);
				}
				else
				{
					/* ver 1.1.0 */
					$value = trim($value);
					$needed = array_filter(array_map('trim',explode(',',$needle)));
					foreach($needed as $need)
					{
						if(strtolower($need)==strtolower($value))
						{
							return $value;
						}
					}
				}
			}
		}
		return false;
    }
	/**
	 * Alias for the_content()
	 *
	 * @since    4.2.0
	 */
	private function the_content($content='') {
		if(empty($content)) return '';
		
		$content = do_shortcode($content);
		$content = apply_filters('the_content', $content);
		return $content;
    }

}